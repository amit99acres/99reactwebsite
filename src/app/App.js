import React, { Component } from 'react'
import { Route, Switch }    from 'react-router-dom';
import routes               from './Routes';
import PageNotFound         from './pages/PageNotFound';

class App extends Component {
  render() {
    const { viewType='desktop' } = this.props;
    // console.log('App JS =>', viewType, this.props);
    return (
      <div>
        <Switch>
          {routes.map(({ path, exact, component: Component, ...rest }) => (
            <Route key={path} path={path} exact={exact} render={(props) => (
              <Component {...props} {...rest} viewType={viewType} />
            )} />
          ))}
          <Route render={(props) => <PageNotFound {...props} /> } />
        </Switch>
      </div>
    )
  }
}

export default App;