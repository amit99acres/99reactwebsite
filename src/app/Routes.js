import HomePage         from './pages/home';
import PdPage           from './pages/pd'; 
import SearchPage           from './pages/searchPg'; 

const Routes =  [ 
  {
    path: '/',
    exact: true,
    component: HomePage
  }
  , {
    path: '/pd/:id',
    component: PdPage
  }
  , {
    path: '/search/:id',
    component: SearchPage
  }
]

export default Routes;