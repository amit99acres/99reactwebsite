const fieldsConfig = {
  "identityConfig": {
    type: "radio"
    , name: "identity"
    , label: "You are"
    , options: [{
      "id": "INDIVIDUAL",
      "label": "Individual" 
    }, {
      "id": "FIRM",
      "label": "Firm" 
    }]
    , constraints: {required:!0}
  }
  , "nameConfig": {
    type: "text"
    , name: "name"
    , placeholder: "Name"
    , constraints: {required:!0, minLength:3, maxLength:25}
  }
  , "emailConfig": {
    type: "text"
    , name: "email"
    , placeholder: "Email"
    , constraints: {required:!0, minLength:8, maxLength:50, isValidEmail:!0}
  }
  , "phoneNumberConfig": {
    type: "text"
    , name: "phone"
    , placeholder: "Phone Number"
    , constraints: {required:!0, minLength:10, maxLength:13, isValidNumber:!0}
  }
  , "messageConfig": {
    type: "textarea"
    , name: "message"
    , placeholder: "I am interested in this Property."
    , constraints: {minLength:5, maxLength:400}
  }
  , "shareMobileConfig": {
    type: "checkbox"
    , name: "share_mobile_info" 
    , options: [{
      "id": "Y",
      "label": "Allow 99acres & affiliates to contact me" 
    }]
    , constraints: {}
  }
}

export default fieldsConfig;