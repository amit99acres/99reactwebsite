import React                    from 'react'; 
import fieldsConfig             from './fieldsConfig';
import CustomInput              from '../../core/CustomInput/';
import CheckBoxRadioGroup       from '../../core/CheckBoxRadioGroup/';
import CustomTextarea           from '../../core/CustomTextarea/';
import ErrorBoundary			      from '../../common/ErrorBoundary';
import './component.css';

class DealerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      identity: '',
      name: '',
      email: '',
      phoneNumber: '',
      message: ''
    };
  } 

  handleNameBlur = (e) => {
    console.log('Blurrrrrrrrrrrrrrrrrrrrrrrrr => Function on Name Field also called. ', e);
  }

  handleFormSubmit= (e) => {
    e.preventDefault();
  } 

  render() {
    // console.log('dealerForm =>', this.props, this.state);
    const {identityConfig, nameConfig, emailConfig, phoneNumberConfig, messageConfig, shareMobileConfig} = fieldsConfig;

   //  const fieldNodes = Object.keys(fieldsConfig).map((key) => {
   //    // console.log('key =>', key);
   //    const fieldObj = fieldsConfig[key];
   //    if(fieldObj['type'] != 'text')
   //      return;
   //    return (<ErrorBoundary key={key}>
   //      <CustomInput
   //        name={fieldObj['name']}
   //        value={this.state.name}
   //        constraints={fieldObj['constraints']}
   //        placeholder={fieldObj['placeholder']}
   //        blurFunc={this['handle' + fieldObj['name'] + 'Blur']} 
   //        changeFunc={this['handle' + fieldObj['name'] + 'Change']} />
			// </ErrorBoundary>)  
   //  });
    
    return (
      <form id="enquiryForm">
        <ErrorBoundary>
          <CheckBoxRadioGroup
            name={identityConfig['name']}
            label={identityConfig['label']}
            value={this.state.name}
            constraints={identityConfig['constraints']}
            options={identityConfig['options']}
            changeFunc={this['handle' + identityConfig['name'] + 'Change']} />
        </ErrorBoundary>
        <div class="inputSet">
          <ErrorBoundary>
            <CustomInput
              name={nameConfig['name']}
              value={this.state.name}
              constraints={nameConfig['constraints']}
              placeholder={nameConfig['placeholder']}
              blurFunc={nameConfig['handle' + nameConfig['name'] + 'Blur']} 
              changeFunc={nameConfig['handle' + nameConfig['name'] + 'Change']} />
          </ErrorBoundary>
          <ErrorBoundary>
            <CustomInput
              name={emailConfig['name']}
              value={this.state.name}
              constraints={emailConfig['constraints']}
              placeholder={emailConfig['placeholder']}
              blurFunc={emailConfig['handle' + emailConfig['name'] + 'Blur']} 
              changeFunc={emailConfig['handle' + emailConfig['name'] + 'Change']} />
          </ErrorBoundary>
          <ErrorBoundary>
            <CustomInput
              name={phoneNumberConfig['name']}
              value={this.state.name}
              constraints={phoneNumberConfig['constraints']}
              placeholder={phoneNumberConfig['placeholder']}
              blurFunc={phoneNumberConfig['handle' + phoneNumberConfig['name'] + 'Blur']} 
              changeFunc={phoneNumberConfig['handle' + phoneNumberConfig['name'] + 'Change']} />
          </ErrorBoundary>
        </div> 
        <ErrorBoundary>
          <CustomTextarea
            name={messageConfig['name']}
            rows={messageConfig['rows']}
            value={this.state.name}
            constraints={messageConfig['constraints']}
            placeholder={messageConfig['placeholder']}
            changeFunc={messageConfig['handle' + messageConfig['name'] + 'Change']} />
        </ErrorBoundary> 
        <div>
          <ErrorBoundary>
            <CheckBoxRadioGroup
              name={shareMobileConfig['name']}
              label={shareMobileConfig['label']}
              value={this.state.name}
              constraints={shareMobileConfig['constraints']}
              options={shareMobileConfig['options']}
              type="checkbox"
              changeFunc={this['handle' + shareMobileConfig['name'] + 'Change']} />
          </ErrorBoundary>
        </div>
      </form>
    );
  }
}

export default DealerForm;