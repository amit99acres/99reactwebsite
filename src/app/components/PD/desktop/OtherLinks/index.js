import React          from 'react'; 
import './component.css';

class OtherLinks extends React.Component {
	render() {
		return ( 
      <div>
        <div class="contentInnerWrap" id="otherLinks">
          <div class="block">
            <div class="heading grey f16 sBold">Hot Properties in nearby localities</div>
            <ul class="listNone pb10">
              <li><a href="" target="_blank" title="1 BHK Apartments in Sector-71 Noida">1 BHK Apartments <br />in Sector-71 Noida</a></li>
              <li><a href="" target="_blank" title="1 BHK Apartments in Sector-121 Noida">1 BHK Apartments <br />in Sector-121 Noida</a></li>
            </ul>
          </div>
          <div class="block">
            <div class="heading grey f16 sBold">Most Searched Properties</div>
            <ul class="listNone pb10">
              <li><a href="" title="Flats in Noida">Flats <br />in Noida</a></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default OtherLinks;