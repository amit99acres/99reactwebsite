import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class ListMapView extends Component{
    constructor(props) {
      super(props);
}

	render() {
    return (		
        <div class="listMapCont" id="container">
            <div class="switch" id="switch"></div>
            <div class="label left">List View</div>
            <div class="label right"><a id="lf_map_view" class="map-view-button" href="/map-view-new-projects-in-noida-ffid?orig_property_type=23&amp;class=O&amp;search_type=QS&amp;np_layout=xid_property_layout&amp;frm_srp=1&amp;clsrc=owner_tab">Map View</a></div>
        </div>
    	);
  }
}

export default ListMapView;