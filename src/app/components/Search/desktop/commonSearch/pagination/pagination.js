import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class Pagination extends Component{
    constructor(props) {
      super(props);
     
}
	
	render() {
    return (		
       <div class="pgdiv">
    
        <span class="pgdis"><span>&lt;&lt;</span>Previous</span>
            <span name="page" value="1" class="pgdes">1</span>
    
            <a href="/property-in-noida-ffid-page-2" name="page" value="2" class="pgsel">2</a>
        
            <a href="/property-in-noida-ffid-page-3" name="page" value="3" class="pgsel">3</a>
       
            <a href="/property-in-noida-ffid-page-4" name="page" value="4" class="pgsel">4</a>
        
            <a href="/property-in-noida-ffid-page-5" name="page" value="5" class="pgsel">5</a>
        
            <a href="/property-in-noida-ffid-page-6" name="page" value="6" class="pgsel">6</a>
       
            <a href="/property-in-noida-ffid-page-7" name="page" value="7" class="pgsel">7</a>
        
            <a href="/property-in-noida-ffid-page-8" name="page" value="8" class="pgsel">8</a>
         
            <a href="/property-in-noida-ffid-page-9" name="page" value="9" class="pgsel">9</a>
         
            <a href="/property-in-noida-ffid-page-10" name="page" value="10" class="pgsel">10</a>
       
            ....
        
        <a href="/property-in-noida-ffid-page-630" name="page" value="630" class="pgsel">630</a>
     
        <a href="/property-in-noida-ffid-page-631" name="page" value="631" class="pgsel">631</a>
    
        <input type="hidden" id="button_next" value="2" />&nbsp;<a href="/property-in-noida-ffid-page-2" class="pgselActive" name="nextbutton" value="Next »">Next <span>&gt;&gt;</span></a>
    
</div>  
    	);
  }
}

export default Pagination;