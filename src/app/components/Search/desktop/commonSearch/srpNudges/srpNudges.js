import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class SrpNudges extends Component{
  constructor(props) {
    super(props);
}

  render() {
    const {stickyCondtn} = this.props; 
    // console.log(this.props.stickyCondtn);
    const {rera, status, possession, offer, pmay} = this.props.data;
    var clsName = this.props.stickyCondtn == true ? 'nudgeFxd' : ''; 
    var tempCls = this.props.stickyCondtn == true ? 'tempDivFxd' : '';
    
    return (		
        <div id="srp_tab_dtl" >
            <div class="boxSize" id="LF_TABS">
            <div class="tempDiv"></div>
            <div class={`srpfxdCnt __srpnudg ${clsName}`}>
                <div class="newTgWrp">
                    <div class="srpTpNudgWrp autoSlideWrp __autoWrp" >
                        <div class="autoSlideFlm __autoFilm" id="nudgeContainer">
                                                <div class="_srpNudgeEl srpTpNudg " id="nudge_lf_rerastatus" data-paramname="rerastatus" data-paramvalue="REGISTERED" data-isfilter="true" data-filterid="lf_rerastatus" data-isselected="0" data-track="RERA" slide-item="">
                                <div>{ReactHtmlParser(rera)}</div> 
                            </div>
                                                <div class="_srpNudgeEl srpTpNudg " id="nudge_PossessionIN_R2M" data-paramname="np_search_type" data-paramvalue="R2M" data-isfilter="true" data-filterid="PossessionIN_R2M" data-isselected="0" data-track="R2M" slide-item="">
                                <div>{ReactHtmlParser(status)}</div> 
                            </div>
                                                <div class="_srpNudgeEl srpTpNudg " id="nudge_PossessionIN_2018" data-paramname="np_search_type" data-paramvalue="2018" data-isfilter="true" data-filterid="PossessionIN_2018" data-isselected="0" data-track="2018" slide-item="">
                                <div>{ReactHtmlParser(possession)}</div> 
                            </div>
                                                <div class="_srpNudgeEl srpTpNudg " id="nudge_lf_npadv" data-paramname="npadv" data-paramvalue="Y" data-isfilter="false" data-filterid="lf_npadv" data-isselected="0" data-track="NPADV" slide-item="">
                                <div>{ReactHtmlParser(offer)}</div> 
                            </div>
                                                <div class="_srpNudgeEl srpTpNudg " id="nudge_lf_pmay" data-paramname="pmay" data-paramvalue="Y" data-isfilter="false" data-filterid="lf_pmay" data-isselected="0" data-track="PMAY" slide-item="">
                                <div>{ReactHtmlParser(pmay)}</div> 
                            </div>
                                    </div>
                      </div>
                      <div id="prev" class="prevArw __nxtPrevClk"></div>
                      <div id="nxt" class="nxtArw __nxtPrevClk hide"></div>
                  </div>
              </div>
              </div>
          </div>   
    	);
  }
}

export default SrpNudges;