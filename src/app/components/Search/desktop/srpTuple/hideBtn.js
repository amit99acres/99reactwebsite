import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

class SrpHideBtn extends Component{
    constructor(props) {
      super(props);
      this.srpHideClk = this.srpHideClk.bind(this);
}

srpHideClk =(e) =>{
    var thisNd = e.target;
    thisNd.nextSibling.style.display = "block";
}
	
	render() {
    return (		
        <span>
    		 <i class="__srpDD hideProp" onClick={this.srpHideClk} title="Hide this property">Hide</i>        
               <div>
                    <a href="#"><i></i><span class="b">Hide Project <span class="optnSpn">Hide all properties in Urban Casa</span></span></a>
                    <a href="#"><i></i><span class="b">Hide Project <span class="optnSpn">Hide all properties in Urban Casa</span></span></a>
                    <a href="#"><i></i><span class="b">Hide Project <span class="optnSpn">Hide all properties in Urban Casa</span></span></a>
                </div>  
        </span>       
    	);
  }
}

export default SrpHideBtn;