import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

class HideCoachMrk extends Component{
	
	render() {
        return (
        		 <div class="hideCoachMrk __hideCm">
                    <div class="spacer20"></div>
                    <div>Don't like a Property?</div>
                    <div class="spacer20"></div>
                    <div class="f16"><b>Hide properties of a project or any dealer </b>that you may not be interested in.</div>
                    <i class="arrowHide"></i>
                    <div class="spacer10"></div>
                    <div class="okBtn __okBtn"><input type="button" value="OK, GOT IT"></input></div>
                     <div class="spacer10"></div>
                     <div class="hideBtn f12">Hide</div>
                </div>
        );
    }
}

export default HideCoachMrk;