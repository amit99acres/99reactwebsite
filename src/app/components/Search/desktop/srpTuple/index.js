import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import HideCoachMrk from './hideCoachmark';
import ShortlistIcn from './shortlistIcn';
import SrpHideBtn from './hideBtn';
import SrpPhotos from './srpPhotos';
import SrpBtnBlue from './srpBtnBlue';
import SrpBtnWhite from './srpBtnWhite';

import './component.css';

class SrpTuple extends Component{
  static propTypes = {
    data: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string.isRequired,
      limit: PropTypes.number
    })
  };

  render() {
    const {srpAddress, srpPropName, srpPriceRange, srpPropArea, srpSpecs, srpPropStatus, srpTotalArea, srpPropBuildArea, srpSpecDetails, srpPropPostedOn, srpPropOwnerName, srpPropPostedBy, description} = this.props.data;
    if(description == undefined)
      return null;

    return (
    <div class="srpWrap srpNw_Tpl  ft_list">
      <div> 
      <SrpPhotos />     
          <div class="srpNw_tble ">
            <table>
                <tbody><tr>
                    <th colSpan={4} class="_srpttl">
                        <a target="_blank" href="/2-bhk-bedroom-apartment-flat-for-sale-in-express-zenith-sector-77-noida-990-sq-ft-to-1310-sq-ft-npspid-H31182581">
                        <span class="srpttl" title="₹ 46.52 Lac to 61.56 Lac 2 BHK Residential Apartment in Sector-77 Noida">{ReactHtmlParser(srpAddress)}</span></a>
                    </th>
                </tr>
                <tr>
          <td colSpan={4}>   
                    <a data-bid="9806" target="_blank" class="sName" title="Express Zenith ,Noida" href="https://setup1.infoedge.com/express-zenith-sector-77-noida-npxid-r4376?src=SRP">
              {ReactHtmlParser(srpPropName)}
            </a>                        
                 </td>
                </tr>
                <tr>
                    <td>
                        <span class="srpNw_price ">{ReactHtmlParser(srpPriceRange)}</span>
                    </td>
                    <td class="_auto_areaValue">
                      {ReactHtmlParser(srpPropArea)}
                    </td>         
            <td class="_auto_bedroom"><b>{ReactHtmlParser(srpSpecs)}</b></td>          
                      <td class="_auto_possesionLabel">                       
                       {ReactHtmlParser(srpPropStatus)}
                    </td>
                </tr>
                <tr>
                    <td class="_auto_ppu_area">{ReactHtmlParser(srpTotalArea)}</td>
                    <td class="_auto_areaType">{ReactHtmlParser(srpPropBuildArea)}
                    </td>
                    <td class="_auto_bath_balc_roadText">{ReactHtmlParser(srpSpecDetails)}
          </td>
                    <td class="srp_TgWrp">                    
                        <div class="srp_nwBook">New Booking</div>                    
                    </td>
                </tr>
                <tr>
                    <td colSpan={4}>
                        <div class="srpNw_desc __srpNw_desc">
                         {ReactHtmlParser(description)}
                           <span class="srp_ml __srpNwLess "> - Less</span>
                        </div>
                        <span class="srp_ml __srpNwMore "> + more</span>
                    </td>
                </tr>
                </tbody></table>
      <div> 
              <ShortlistIcn />
              <SrpHideBtn />                       
        </div>
        </div>
    </div>    
    <div>       
        <span class="srpNw_dlrNme">Posted on {ReactHtmlParser(srpPropPostedOn)} by <a href="#" title="Namrata">{ReactHtmlParser(srpPropOwnerName)} </a>({ReactHtmlParser(srpPropPostedBy)})</span>
        <SrpBtnBlue />
        <SrpBtnWhite />        
        <div class="clearfix"></div> 
    </div>       
   <HideCoachMrk />
   <input  type="hidden" value="ON" />
</div>
      );
  }
}

export default SrpTuple;