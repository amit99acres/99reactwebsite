import React, {Component} from 'react';
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

class ShortlistIcn extends Component{
	
	render() {
    return (
		<i title="Shortlist this property"></i>
    	);
  }
}

export default ShortlistIcn;