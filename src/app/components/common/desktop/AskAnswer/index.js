import React          from 'react'; 
import './component.css';

class AskAnswer extends React.Component {
	render() {
		return ( 
			<div>
				<div class="contentInnerWrap pb10 grey" id="askAnswer">
					<h6 class="sBold">Ask & Answer</h6>
					<div class="block pb15">
						<div class="heading">Ask a Question</div>
						<div>
							<textarea class="fL f13" cols="20" rows="7" placeholder="Have a question about Sector-70 Noida? Ask 2793 experts and 2788 buyers active in Noida"></textarea>
							<input value="Ask Now" type="submit" class="blueSubmit f14 btn sBold" />
						</div>
					</div>
					<div class="block clr">
						<div class="heading">Recent Questions in Sector-79 Noida</div>
						<ul class="listNone">
							<li>
								<a href="" target="_blank">Mayank </a>
								<a href="" target="_blank">
									<span class="grayFont">Please tell me about the features which the builder has assured to offer in this project for the safety of residents? Is Locality safe to reside?</span>
								</a>
								<div class="location">
									in <a href="/ask-real-estate-new-projects">New Projects</a> - <a href="">Sector-79 Noida</a>, <a href="">Noida</a>
								</div>
							</li>
							<li>
								<a href="" target="_blank">Sanjeev Verma </a>
								<a href="" target="_blank">
									<span class="grayFont">Can I take action against 99 acre and muneet Khan who has wrongly posted my property for sale .I am mentally harrassed and don't know to whom should I contact ? I have only requested for rent . Now muneet is not picking up calls . Sanjeev Verma , sector 70, noida</span>
								</a>
								<div class="location">
									in <a href="/ask-real-estate-new-projects">New Projects</a> - <a href="">Sector-79 Noida</a>, <a href="">Noida</a>
								</div>
							</li>
							<li>
								<a href="" target="_blank">Jasmeet Singh </a>
								<a href="" target="_blank">
									<span class="grayFont">i am looking to invest in this property in the next 2 months.I Want to know is it good to invest in this project ?can i see a possible growth in the long term?</span>
								</a>
								<div class="location">
									in <a href="">Residential</a>, <a href="">Noida</a>
								</div>
							</li>
						</ul>
						<a href="" target="_blank" class="moreQuestions">More Questions in Noida </a>
					</div>
				</div>
			</div> 
		);
	}
}

export default AskAnswer;