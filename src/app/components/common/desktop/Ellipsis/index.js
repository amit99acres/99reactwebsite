import React          from 'react';
import TabWrp from './tabs/index';

import './component.css';

class CommnEllipsis extends React.Component {
	 constructor(props) {
    	super(props);
    	this.showHotAreas = this.showHotAreas.bind(this);
    	this.tabClick = this.tabClick.bind(this);
    	this.flag = true;
	}

	 showHotAreas =(e) =>{
	    var div = e.target.nextSibling;
	   
	    div.classList.toggle('open');

	    let bgLyr = this.flag ? "on" : "off";
	    this.props.onBgOn(bgLyr);
	    // console.log(bgLyr);
       	this.flag = !this.flag;
	    

	    // this.props.onChangem(this.state.mdLayer);
	    // this.hamInClose();    
	  }

	  tabClick =(e) =>{
	  	var x = e.target;
	  	console.log(x)
	  }

	render() {		
		return ( 
			<div>
				<div id="blueBand" class="headerFloatRight showi elipAdjst">
				   <div id="sBluBar" class="rel newBar hotArea showBlu" >
				      <i id="hot-area-projects" class="hdrIcn-ellipses iconS _showTip" onClick={this.showHotAreas}></i>
				      
						<TabWrp />



				   </div>
				</div>

			</div>
		);
	}
}

export default CommnEllipsis;