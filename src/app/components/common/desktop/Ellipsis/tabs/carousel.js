import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Carousel extends Component{
		constructor(props) {
	 	 super(props);
	   this.state={	   
	   	 slider: ["first", "second", "third", "fourth", "fifth"],
      	 activeIndex: 1,
      	 left: 0
	   }
	   
	 this.clickIndicator = this.clickIndicator.bind(this);
	 
}

clickIndicator = (e) => {
    this.setState({
      activeIndex: parseInt(e.target.textContent),
      left: this.props.sliderWidth - parseInt(e.target.textContent) * this.props.sliderWidth
    })
  }

  render() {
  		var style = {
      left: this.state.left,
      width: this.props.sliderWidth,
      height: this.props.sliderHeight
    }

    return (
    		 <div className="tabWrap">
		        <div  className="slider-wrapper">
		        <ul className="slider">
			        {this.state.slider.map(function(item,index) {
			          return (
			          <li style={style} className={index+1 === this.state.activeIndex ? 'slider-item' : 'others'}>
			          	

			          	<table className="ellipsList1" id="sliderButton-0-screen-1">    
				        <tbody>
				        <tr>          
				           <td className="srpHotArea">                      
				            <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                <a href="https://www.99acres.com/property-in-noida-extension-noida-ffid" itemprop="url">
				                    <span itemprop="name">Property in Noida Extension</span>
				                </a>
				            </span>
				                </td>            
				                <td className="srpHotArea">           
				                <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                    <a href="https://www.99acres.com/property-in-sector-137-noida-ffid" itemprop="url">
				                        <span itemprop="name">Property in Sector-137 Noida</span>
				                    </a>
				                </span>  
				                </td>            
				                <td className="srpHotArea">           
				                <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                    <a href="https://www.99acres.com/property-in-sector-78-noida-ffid" itemprop="url">
				                        <span itemprop="name">Property in Sector-78 Noida</span>
				                    </a>
				                </span>    
				            </td>            
				        </tr>    
				        <tr>
				            <td className="srpHotArea">                
				                <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                    <a href="https://www.99acres.com/property-in-sector-128-noida-ffid" itemprop="url">
				                        <span itemprop="name">Property in Sector-128 Noida</span>
				                    </a>
				                </span>                 
				                </td>            
				            <td className="srpHotArea">                    
				                <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                    <a href="https://www.99acres.com/property-in-sector-150-noida-ffid" itemprop="url">
				                        <span itemprop="name">Property in Sector-150 Noida</span>
				                    </a>
				                </span>   
				                </td>
				            <td className="srpHotArea">                  
				                <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                    <a href="https://www.99acres.com/property-in-sector-121-noida-ffid" itemprop="url">
				                        <span itemprop="name">Property in sector-121 Noida</span>
				                    </a>
				                </span>     
				                </td>            
				        </tr>    
				        <tr>
				        <td className="srpHotArea">                     
				            <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                <a href="https://www.99acres.com/property-in-sector-168-noida-ffid" itemprop="url">
				                    <span itemprop="name">Property in Sector-168 Noida</span>
				                </a>
				            </span>      
				                </td>            
				                <td className="srpHotArea">                     
				                    <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                        <a href="https://www.99acres.com/property-in-sector-75-noida-ffid" itemprop="url">
				                            <span itemprop="name">Property in Sector-75 Noida</span>
				                        </a>
				                    </span>      
				                </td>            
				                <td className="srpHotArea">                    
				                    <span itemscope="" itemtype="https://schema.org/SiteNavigationElement">
				                        <a href="https://www.99acres.com/property-in-sector-74-noida-ffid" itemprop="url">
				                            <span itemprop="name">Property in Sector-74 Noida</span>
				                        </a>
				                    </span>    
				                </td>            
				        </tr>    
				</tbody></table>



			          </li>
			         
			          )
			        },this)
			        }
			        </ul>
			        </div>
			        <div className="indicators-wrapper">
			          <ul className="indicators">
			         {this.state.slider.map(function(item,index) {
			          return (
			          <li className={index+1 === this.state.activeIndex ? 'active-indicator' : ''}onClick={this.clickIndicator}>{index+1}</li>
			          )
			        },this)
			        }
		          </ul>
		        </div>
      		</div>

      )
  }
}

export default Carousel;




