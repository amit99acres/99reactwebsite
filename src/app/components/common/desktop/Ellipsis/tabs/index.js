import React, {Component} from 'react';

import Tabs from './tabs';
import InnerTabs from './innerTabs';
import Carousel from './carousel';

class TabWrp extends Component{
	constructor(props) {
	  super(props);

	  // this.oninnrTab = this.oninnrTab.bind(this);
	}

		// oninnrTab (status){
  //       this.setState({activeTab: status});
  //       console.log(status);
  // }
  render() {
  	
    return (  

    		
		      
		      <Tabs>
		        <div label="Hot Projects">
		          <table cellPadding={3}>
    <tbody>
        <tr>
            <td colSpan={2} height="10"></td>
        </tr>
         <tr className="sBluDrp" id="divproj25">
             <th ><a href="https://www.99acres.com/new-projects-in-delhi-ncr-ffid?src=HOTPROJ" trackval="NP_City" trackact="Delhi">Delhi</a></th>
            <td  id="proj25">
                <div className="elipLinks">
                                        <a href="https://www.99acres.com/antriksh-urban-greek-l-zone-delhi-dwarka-npxid-r155149?src=HOTPROJ" trackval="NP_Project" trackact="Antriksh Urban Greek"> Antriksh Urban Greek</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/revanta-smart-residency-l-zone-delhi-dwarka-npxid-r93954?src=HOTPROJ" trackval="NP_Project" trackact="Revanta Smart Residency"> Revanta Smart Residency</a><span className="pipeSign"></span> 
                                        <a  href="https://www.99acres.com/revanta-welfare-society-g-t-karnal-road-delhi-north-npxid-r23112?src=HOTPROJ" trackval="NP_Project" trackact="Revanta Welfare Society"> Revanta Welfare Society</a><span className="pipeSign"></span> 
                                        <a  href="https://www.99acres.com/victorian-privilege-l-zone-delhi-dwarka-npxid-r180689?src=HOTPROJ" trackval="NP_Project" trackact="Victorian Privilege"> Victorian Privilege</a></div>
                                                <div className="elpTabViewAll"><a href="https://www.99acres.com/new-projects-in-delhi-ncr-ffid">View All</a></div>
                <div className="clr"></div>
            </td>
         </tr>
         <tr class="rowdisplay">
            <td colSpan={2} height="10" className="bdrJq"></td>
         </tr>
         <tr>
            <td colSpan={2} height="10"></td>
         </tr>
          <tr>
            <td colSpan={2} height="10" ></td>
        </tr>
         <tr class="clr" id="divproj15">
             <th ><a href="https://www.99acres.com/new-projects-in-noida-ffid?src=HOTPROJ" trackval="NP_City" trackact="Noida">Noida</a></th>
            <td  id="proj15">
                <div className="elipLinks">
                        <a  href="https://www.99acres.com/jm-orchid-sector-76-noida-npxid-r35142?src=HOTPROJ" trackval="NP_Project" trackact="JM Orchid"> JM Orchid</a><span className="pipeSign"></span> 
                        <a  href="https://www.99acres.com/samridhi-luxuriya-avenue-sector-150-noida-npxid-r205109?src=HOTPROJ" trackval="NP_Project" trackact="Samridhi Luxuriya Avenue"> Samridhi Luxuriya Avenue</a><span className="pipeSign"></span> 
                        <a href="https://www.99acres.com/amrapali-silicon-city-sector-76-noida-npxid-r769?src=HOTPROJ" trackval="NP_Project" trackact="Amrapali Silicon City"> Amrapali Silicon City</a><span className="pipeSign"></span> 
                        <a  href="https://www.99acres.com/supertech-eco-village-3-noida-extension-npxid-r978?src=HOTPROJ" trackval="NP_Project" trackact="Supertech Eco Village 3"> Supertech Eco Village 3</a><span className="pipeSign"></span> 
                        <a href="https://www.99acres.com/amrapali-princely-estate-sector-76-noida-npxid-r798?src=HOTPROJ" trackval="NP_Project" trackact="Amrapali Princely Estate"> Amrapali Princely Estate</a></div>
                <div className="elipLinks"><a href="https://www.99acres.com/ats-pristine-sector-150-noida-npxid-r10130?src=HOTPROJ" trackval="NP_Project" trackact="ATS Pristine"> ATS Pristine</a><span className="pipeSign"></span> 
                        <a href="https://www.99acres.com/paramount-floraville-sector-137-noida-npxid-r736?src=HOTPROJ" trackval="NP_Project" trackact="Paramount Floraville"> Paramount Floraville</a><span className="pipeSign"></span> 
                        <a href="https://www.99acres.com/aditya-celebrity-homes-sector-76-noida-npxid-r772?src=HOTPROJ" trackval="NP_Project" trackact="Aditya Celebrity Homes"> Aditya Celebrity Homes</a><span className="pipeSign"></span> 
                        <a href="https://www.99acres.com/rg-residency-sector-120-noida-npxid-r5968?src=HOTPROJ" trackval="NP_Project" trackact="RG Residency"> RG Residency</a><span className="pipeSign"></span> 
                        <a  href="https://www.99acres.com/elite-homz-sector-77-noida-npxid-r6598?src=HOTPROJ" trackval="NP_Project" trackact="Elite Homz"> Elite Homz</a><span className="pipeSign"></span> 
                        <a href="https://www.99acres.com/exotica-dreamville-noida-extension-npxid-r4514?src=HOTPROJ" trackval="NP_Project" trackact="Exotica Dreamville"> Exotica Dreamville</a></div>
                 <div className="elpTabViewAll"><a href="https://www.99acres.com/new-projects-in-noida-ffid">View All</a></div>
                <div className="clr"></div>
                </td>
                </tr>
                <tr className="rowdisplay">
                    <td colSpan={2} height="10" className="bdrJq"></td>
                </tr>
             <tr>
            <td colSpan={2} height="10"></td>
        </tr>
         <tr>
            <td colSpan={2} height="10" ></td>
        </tr>
         <tr class="clr" id="divproj21">
             <th ><a href="https://www.99acres.com/new-projects-in-greater-noida-ffid?src=HOTPROJ" trackval="NP_City" trackact="Greater Noida">Greater Noida</a></th>
            <td  id="proj21">
                <div className="elipLinks">
                <a  href="https://www.99acres.com/amaatra-homes-vaidpura-greater-noida-npxid-r10822?src=HOTPROJ" trackval="NP_Project" trackact="Amaatra Homes"> Amaatra Homes</a><span className="pipeSign"></span> 
                <a  href="https://www.99acres.com/panchsheel-greens-2-sector-16-gr-noida-greater-noida-npxid-r1084?src=HOTPROJ" trackval="NP_Project" trackact="Panchsheel Greens 2"> Panchsheel Greens 2</a><span className="pipeSign"></span> 
                <a  href="https://www.99acres.com/gulshan-bellina-sector-16-gr-noida-greater-noida-npxid-r5012?src=HOTPROJ" trackval="NP_Project" trackact="Gulshan Bellina">Gulshan Bellina</a><span className="pipeSign"></span> 
                <a href="https://www.99acres.com/supertech-upcountry-yamuna-expressway-greater-noida-npxid-r1094?src=HOTPROJ" trackval="NP_Project" trackact="Supertech Upcountry"> Supertech Upcountry</a><span className="pipeSign"></span> 
                <a  href="https://www.99acres.com/paramount-emotions-greater-noida-west-npxid-r7884?src=HOTPROJ" trackval="NP_Project" trackact="Paramount Emotions"> Paramount Emotions</a></div>
                 <div className="elpTabViewAll"><a href="https://www.99acres.com/new-projects-in-greater-noida-ffid">View All</a></div>
                <div className="clr"></div>
                </td>
                </tr>
                <tr className="rowdisplay">
                    <td colSpan={2} height="10" className="bdrJq"></td>
                </tr>
             <tr>
            <td colSpan={2} height="10"></td>
        </tr>
         <tr>
            <td colSpan={2} height="10" ></td>
        </tr>
         <tr class="clr" id="divproj17">
             <th class="in0City"><a href="https://www.99acres.com/new-projects-in-gurgaon-ffid?src=HOTPROJ" trackval="NP_City" trackact="Gurgaon">Gurgaon</a></th>
            <td  id="proj17">
                <div className="elipLinks">
                                        <a href="https://www.99acres.com/dlf-park-place-sector-54-gurgaon-npxid-r8?src=HOTPROJ" trackval="NP_Project" trackact="DLF Park Place"> DLF Park Place</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/bestech-park-view-spa-sector-47-gurgaon-npxid-r603?src=HOTPROJ" trackval="NP_Project" trackact="Bestech Park View Spa">Bestech Park View Spa</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/jmd-megapolis-sector-48-gurgaon-npxid-c1618?src=HOTPROJ" trackval="NP_Project" trackact="JMD Megapolis"> JMD Megapolis</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/bptp-park-prime-sector-66-gurgaon-npxid-r680?src=HOTPROJ" trackval="NP_Project" trackact="BPTP Park Prime"> BPTP Park Prime</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/ireo-victory-valley-sector-67-gurgaon-npxid-r871?src=HOTPROJ" trackval="NP_Project" trackact="Ireo Victory Valley"> Ireo Victory Valley</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/orchid-island-sector-51-gurgaon-npxid-r8010?src=HOTPROJ" trackval="NP_Project" trackact="Orchid Island"> Orchid Island</a></div>
                                                <div className="elpTabViewAll"><a href="https://www.99acres.com/new-projects-in-gurgaon-ffid">View All</a></div>
                <div className="clr"></div>
                </td>
                </tr>
                <tr className="rowdisplay">
                    <td colSpan={2} height="10" className="bdrJq"></td>
                </tr>
             <tr>
            <td colSpan={2} height="10"></td>
        </tr>
         <tr>
            <td colSpan={2} height="10" ></td>
        </tr>
         <tr class="clr" id="divproj23">
             <th ><a href="https://www.99acres.com/new-projects-in-ghaziabad-ffid?src=HOTPROJ" trackval="NP_City" trackact="Ghaziabad">Ghaziabad</a></th>
            <td  id="proj23">
                <div className="elipLinks">
                                        <a  href="https://www.99acres.com/color-homes-nh-24-highway-ghaziabad-npxid-r28072?src=HOTPROJ" trackval="NP_Project" trackact="Color Homes"> Color Homes</a><span className="pipeSign"></span> 
                                        <a  href="https://www.99acres.com/ireo-victory-valley-sector-67-gurgaon-npxid-r871?src=HOTPROJ" trackval="NP_Project" trackact="ATS Advantage"> ATS Advantage</a><span className="pipeSign"></span> 
                                        <a  href="https://www.99acres.com/vvip-addresses-raj-nagar-extension-ghaziabad-npxid-r1207?src=HOTPROJ" trackval="NP_Project" trackact="VVIP Addresses"> VVIP Addresses</a><span className="pipeSign"></span> 
                                        <a  href="https://www.99acres.com/gaur-cascades-raj-nagar-extension-ghaziabad-npxid-r2452?src=HOTPROJ" trackval="NP_Project" trackact="Gaur Cascades"> Gaur Cascades</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/ajnara-integrity-raj-nagar-extension-ghaziabad-npxid-r14386?src=HOTPROJ" trackval="NP_Project" trackact="Ajnara Integrity"> Ajnara Integrity</a><span className="pipeSign"></span> 
                                        <a  href="https://www.99acres.com/rise-organic-homes-nh-24-highway-ghaziabad-npxid-r147935?src=HOTPROJ" trackval="NP_Project" trackact="Rise Organic Homes"> Rise Organic Homes</a></div>
                                                <div className="elpTabViewAll"><a href="https://www.99acres.com/new-projects-in-ghaziabad-ffid">View All</a></div>
                <div className="clr"></div>
                </td>
                </tr>
                <tr className="rowdisplay">
                    <td colSpan={2} height="10" className="bdrJq"></td>
                </tr>
             <tr>
            <td colSpan={2} height="10"></td>
        </tr>
         <tr>
            <td colSpan={2} height="10" ></td>
        </tr>
         <tr class="clr" id="divproj19">
             <th class="in0City"><a href="https://www.99acres.com/new-projects-in-faridabad-ffid?src=HOTPROJ" trackval="NP_City" trackact="Faridabad">Faridabad</a></th>
            <td  id="proj19">
                <div className="elipLinks">
                                        <a href="https://www.99acres.com/srs-residency-sector-88-faridabad-npxid-r2656?src=HOTPROJ" trackval="NP_Project" trackact="SRS Residency"> SRS Residency</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/rps-savana-sector-88-faridabad-npxid-r5438?src=HOTPROJ" trackval="NP_Project" trackact="RPS Savana"> RPS Savana</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/ansal-royal-heritage-sector-70-faridabad-npxid-r2416?src=HOTPROJ" trackval="NP_Project" trackact="Ansal Royal Heritage"> Ansal Royal Heritage</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/rps-palms-sector-88-faridabad-npxid-r26826?src=HOTPROJ" trackval="NP_Project" trackact="RPS Palms"> RPS Palms</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/puri-pratham-sector-84-faridabad-npxid-r31990?src=HOTPROJ" trackval="NP_Project" trackact="Puri Pratham"> Puri Pratham</a><span className="pipeSign"></span> 
                                        <a href="https://www.99acres.com/vatika-mindscapes-sector-27d-faridabad-npxid-c2733?src=HOTPROJ" trackval="NP_Project" trackact="Vatika Mindscapes"> Vatika Mindscapes</a></div>
                                                <div className="elpTabViewAll"><a href="https://www.99acres.com/new-projects-in-faridabad-ffid">View All</a></div>
                <div className="clr"></div>
                </td>
                </tr>
                 <tr>
            <td colSpan={2} height="10" ></td>
        </tr>
        </tbody>
</table>
		         
		        </div>
		        <div label="Hot Areas">
		          
		           <InnerTabs >
		          	<div className="cWrap" label="Property in Noida">
		          		<Carousel sliderWidth="790" sliderHeight="100%"/>
			         
			        </div>
			        <div label="Apartments in Noida">
			          <Carousel sliderWidth="790" sliderHeight="100%"/>
			        </div>
			        <div label="House/Villas in Noida">
			          <Carousel sliderWidth="790" sliderHeight="100%"/>
			        </div>
		          </InnerTabs>
		          
		        </div>
		        <div label="Builders">
<table className="ellipsList1" id="sliderButton-0-screen-1">    
				        <tbody>
				        <tr>          
				           <td className="srpHotArea">                      
				            <span>
				                <a href="https://www.99acres.com/property-in-noida-extension-noida-ffid"  >
				                    <span  >Property in Noida Extension</span>
				                </a>
				            </span>
				                </td>            
				                <td className="srpHotArea">           
				                <span>
				                    <a href="https://www.99acres.com/property-in-sector-137-noida-ffid"  >
				                        <span  >Property in Sector-137 Noida</span>
				                    </a>
				                </span>  
				                </td>            
				                <td className="srpHotArea">           
				                <span>
				                    <a href="https://www.99acres.com/property-in-sector-78-noida-ffid"  >
				                        <span  >Property in Sector-78 Noida</span>
				                    </a>
				                </span>    
				            </td>            
				        </tr>    
				        <tr>
				            <td className="srpHotArea">                
				                <span>
				                    <a href="https://www.99acres.com/property-in-sector-128-noida-ffid"  >
				                        <span  >Property in Sector-128 Noida</span>
				                    </a>
				                </span>                 
				                </td>            
				            <td className="srpHotArea">                    
				                <span>
				                    <a href="https://www.99acres.com/property-in-sector-150-noida-ffid"  >
				                        <span  >Property in Sector-150 Noida</span>
				                    </a>
				                </span>   
				                </td>
				            <td className="srpHotArea">                  
				                <span>
				                    <a href="https://www.99acres.com/property-in-sector-121-noida-ffid"  >
				                        <span  >Property in sector-121 Noida</span>
				                    </a>
				                </span>     
				                </td>            
				        </tr>    
				        <tr>
				        <td className="srpHotArea">                     
				            <span>
				                <a href="https://www.99acres.com/property-in-sector-168-noida-ffid"  >
				                    <span  >Property in Sector-168 Noida</span>
				                </a>
				            </span>      
				                </td>            
				                <td className="srpHotArea">                     
				                    <span>
				                        <a href="https://www.99acres.com/property-in-sector-75-noida-ffid"  >
				                            <span  >Property in Sector-75 Noida</span>
				                        </a>
				                    </span>      
				                </td>            
				                <td className="srpHotArea">                    
				                    <span>
				                        <a href="https://www.99acres.com/property-in-sector-74-noida-ffid"  >
				                            <span  >Property in Sector-74 Noida</span>
				                        </a>
				                    </span>    
				                </td>            
				        </tr>    
				</tbody></table>
		        </div>
		      </Tabs>
		   

      );
  }
}

export default TabWrp;




