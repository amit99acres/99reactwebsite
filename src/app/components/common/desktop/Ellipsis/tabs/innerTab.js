import React, {Component} from 'react';
import PropTypes from 'prop-types';

class InnerTab extends Component{
	static propTypes = {
	    activeTab: PropTypes.string.isRequired,
	    label: PropTypes.string.isRequired,
	    onClick: PropTypes.func.isRequired,
	  };

	  onClick = () => {
	    const { label, onClick } = this.props;
	    onClick(label);
	  }
  render() {
  		const { 
	      onClick,
	      props: {
	        activeTab,
	        label,
	      },
    	} = this;

    let className = 'inn-tab-list-item ';

    if (activeTab === label) {
      className += 'inn-tab-list-active';
    }
    return (  
			 <li 
		        className={className}
		        onClick={onClick}
		      >
		      	<div className="middler"></div>
		        <span>{label}</span>
		      </li>

      );
  }
}

export default InnerTab;




