import React, {Component} from 'react';
import PropTypes from 'prop-types';

import InnerTab from './innerTab';

class InnerTabs extends Component{
	static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
       activeTab: this.props.children[0].props.label,
    };
    // this.tabPersist = '';
    //this.props.children[0].props.label,
  }

  onClickTabItem = (tab) => {
    this.setState({ activeTab: tab });
    // this.tabPersist = tab;
    // this.props.oninnrTab(tab);
    // this.state.activeTab = tab;
     // console.log(tab);
  }
	
  render() {
  		 const {
		      onClickTabItem,
		      props: {
		        children,
		      },
		      state: {
		        activeTab, 
		      }
		    } = this;
		  // console.log(activeTab); 

    return (  

		    <div className="inn-tabs tabs">
		        <ol className="inn-tab-list">
		          {children.map((child) => {
		            const { label } = child.props;

		            return (
		              <InnerTab
		                activeTab={activeTab}
		                key={label}
		                label={label}
		                onClick={onClickTabItem}
		              />
		            );
		          })}
		        </ol>
		        <div className="clr"></div>
		        <div className="inn-tab-content">
		          {children.map((child) => {
		            if (child.props.label !== activeTab) return undefined;
		            return child.props.children;
		          })}
		        </div>
		      </div>

      );
  }
}

export default InnerTabs;




