import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

class Column extends React.Component {
  static propTypes = {
    name: PropTypes.string
    , data: PropTypes.shape({
      title: PropTypes.string
      , list: PropTypes.arrayOf(
        PropTypes.shape({
          SMALL_LABEL: PropTypes.string
        })
      ).isRequired
    })
  };

	render() {
    const {name} = this.props;
    const {title, list} = this.props.data;

    if(typeof list == 'undefined' || list.length == 0)
      return null;
    const liNode = list.map((item, index) => {
      return (<li key={index}>
        <a href={item.URL} title={item.FINAL_LABEL}>{ReactHtmlParser(item.FINAL_LABEL)}</a>
      </li>)
    });

		return ( 
      <div class={'column '+name}>
        <div class="title">{title}</div>
        <ul>
          {liNode} 
        </ul>
      </div>   
		);
	}
}

export default Column;