import React 	         		from 'react'; 

class LastColumn extends React.Component {
  render() {
    return (
      <div class="column col5">
        <div class="title">CONTACT US</div>
        <div class="lightGrey pb10">
          <span class="sBold">
            Toll Free <span class="blueFont">1800 41 99099</span>
          </span>
          <br />
          <span class="f12">Monday - Saturday (9:00AM to 6:00PM IST)</span>
        </div>
        <div class="f13">Email: <a class="blkLink footer_colorBlue" href="mailto:services@99acres.com">feedback@99acres.com</a></div>
        <div class="connect">
          <div class="title">CONNECT WITH US</div>
          <a class="uiSprite fb" href="https://www.facebook.com/99acres/" target="_blank"></a>
          <a class="uiSprite yt" href="https://www.youtube.com/user/99acresindia" target="_blank"></a>
          <a class="uiSprite tw" href="https://twitter.com/99acresIndia" target="_blank"></a>
          <a class="uiSprite gm" href="https://plus.google.com/u/0/+99acres" target="_blank"></a>
          <div class="f11">* India's No.1 Property Portal. Source: Alexa.com</div>
        </div>
        <div class="apps">
          <div class="title">MOBILE APP</div>
          <a class="uiSprite android" title="99acres Android App" target="_blank" href="https://play.google.com/store/apps/details?id=com.nnacres.app&amp;hl=en"></a>
          <a class="uiSprite ios" title="99acres iOS App" target="_blank" href="https://itunes.apple.com/in/app/99acres-property-search/id781765588"></a>
        </div>
      </div>
    )
  }
}

export default LastColumn;