import React 	         		from 'react'; 
import Column		          from './Column'; 
import LastColumn		      from './LastColumn'; 
import ErrorBoundary			from '../../ErrorBoundary';
import { footerLinks }		from '../../../../config/constants';

import './component.css';

class Footer extends React.Component {
	render() {
		const col1 = this.props.data;
		const listObj = Object.assign({}, col1, footerLinks);
		const columnNodes = Object.keys(listObj).map((key) => {
      return (<ErrorBoundary key={key}>
				<Column name={key} data={listObj[key]} />  
			</ErrorBoundary>)  
		});
		
		return (
			<div id="footer">
				<div class="wrap">
					<div class="clrAftr">
						{columnNodes} 
						<LastColumn />
					</div>
					<div class="description f13">
						Usage of 99acres.com to upload content which enables targeting by religion/community/caste/race is prohibited. Please report inappropriate content by writing to us at <a href="mailto:legal@99acres.com?Subject=Report Inappropriate Content">report abuse</a>
					</div>
				</div>
				<div class="bar f13">
					All right reserved - Info Edge (India) Ltd. A <i class="uiSprite vmid naukri"></i> group venture
				</div>
			</div>
		);
	}
}

export default Footer;