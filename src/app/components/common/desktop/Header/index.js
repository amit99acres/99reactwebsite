import React          from 'react'; 
import './component.css';

class Header extends React.Component {
	render() {
		return ( 
			<header class="clrAftr">
				<a id="logo" href="https://www.99acres.com" title="India's No.1 Property Portal">
					<i class="iconS logoIcon"></i>
				</a>
				<div id="buyRentDD">
					<span>Buy</span><i class="iconS vmid downArrow"></i>
				</div>
				<ul class="iconSet listNone">
					<li>
						<a href="">
							<i class="iconS menuIcon"></i>
							<span>Menu</span>
						</a>
					</li>
					<li>
						<a href="" target="_blank">
							<i class="notifySprite dashboardIcon"></i>
							<span>Dashboard</span>
						</a>
					</li>
					<li>
						<a href="" target="_blank">
							<i class="iconS loanIcon"></i>
							<span>Home Loans</span>
						</a>
					</li>
				</ul>
			</header> 
		);
	}
}

export default Header;