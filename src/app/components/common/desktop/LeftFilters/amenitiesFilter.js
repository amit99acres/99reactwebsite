import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class AmenitiesFilter extends React.Component {
	render() {
		return ( 
			<div>
				<div id="lfAmnSec1" class="filter-sub-section">
				    <div class="sub-section-head">Amenities<div id="amn_clear" class="reset-cls chk-clear hide">Clear</div></div>
				    <i class="fiter-icons-sprite minus lfExpCol"></i>
				</div>
				<div id="lfAmnSec2" class="lf-ul-cnt">
				   <ul class="html-checkBox colm2" id="lf_amenitiesList">
				      <li><label for="amenities_21"><input name="amenities" type="checkbox" value="21" id="amenities_21" /><span></span>Lift</label></li>
				      <li><label for="amenities_6"><input name="amenities" type="checkbox" value="6" id="amenities_6" /><span></span>Park</label></li>
				      <li><label for="amenities_12"><input name="amenities" type="checkbox" value="12" id="amenities_12" /><span></span>Gymnasium</label></li>
				      <li><label for="amenities_1"><input name="amenities" type="checkbox" value="1" id="amenities_1" /><span></span>Swimming Pool</label></li>
				      <li><label for="amenities_9"><input name="amenities" type="checkbox" value="9" id="amenities_9" /><span></span>Security Personnel</label></li>
				      <li><label for="amenities_2"><input name="amenities" type="checkbox" value="2" id="amenities_2" /><span></span>Power Backup</label></li>
				      <li><label for="amenities_29"><input name="amenities" type="checkbox" value="29" id="amenities_29" /><span></span>Gas Pipeline</label></li>
				      <li><label for="amenities_4"><input name="amenities" type="checkbox" value="4" id="amenities_4" /><span></span>Parking</label></li>
				      <li><label for="amenities_3"><input name="amenities" type="checkbox" value="3" id="amenities_3" /><span></span>Club house</label></li>
				   </ul>
				</div>
			</div>
		);
	}
}

export default AmenitiesFilter;