import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class AreaFilter extends React.Component {
	render() {
		return ( 
			<div>
				<div class="filter-sub-section">
				<div class="sub-section-head">Area<div id="lf_area_clear" class="reset-cls hide">Clear</div></div>
				    <i class="fiter-icons-sprite minus lfExpCol"></i>
				</div>
				<div>
					<div class="min-max" id="lf_area_input">
					    <div class="min">
					        <input type="text" id="lf_area_min" name="area_min" class="minValue" placeholder="Min" value="" />
					    </div>
					    <div class="hyphen filterHyphen"></div>
					    <div class="max">
					        <input type="text" id="lf_area_max" name="area_max" class="maxValue" placeholder="Max" value="" />
					    </div>
					</div>

					<div class="area-unit-box lf-drpdwn"  val="0">
					    <div name="area_unit" class="get-show-area-unit lf-chk" val="1" id="lf_areaunit_label">Sq.Ft.</div>
					    <i class="fiter-icons-sprite filter-arrowDown"></i>
					    <div class="select-area-unit lf-list lf-minmax" val="0">
					        <div class="lfscrollbar2" ><div class="scrollbar" ><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
					            <div class="viewport "><div class="overview" ><ul id="lf_area_unit"><li class="liSel" name="area_unit" data-val="1" active="">Sq.Ft.</li><li class="" name="area_unit" data-val="2" active="">Sq. Yards</li><li class="" name="area_unit" data-val="3" active="">Sq. Meter</li><li class="" name="area_unit" data-val="4" active="">Grounds</li><li class="" name="area_unit" data-val="5" active="">Aankadam</li><li class="" name="area_unit" data-val="6" active="">Rood</li><li class="" name="area_unit" data-val="7" active="">Chataks</li><li class="" name="area_unit" data-val="8" active="">Perch</li><li class="" name="area_unit" data-val="9" active="">Guntha</li><li class="" name="area_unit" data-val="10" active="">Ares</li><li class="" name="area_unit" data-val="11" active="">Biswa</li><li class="" name="area_unit" data-val="12" active="">Acres</li><li class="" name="area_unit" data-val="13" active="">Bigha</li><li class="" name="area_unit" data-val="14" active="">Kottah</li><li class="" name="area_unit" data-val="15" active="">Hectares</li><li class="" name="area_unit" data-val="16" active="">Marla</li><li class="" name="area_unit" data-val="17" active="">Kanal</li><li class="" name="area_unit" data-val="19" active="">Cents</li></ul></div></div>
					        </div>
					    </div>
					</div>
					<input type="button" id="lf_area_go" value="Go" class="area-unit-go" />
				</div>
			</div>
		);
	}
}

export default AreaFilter;