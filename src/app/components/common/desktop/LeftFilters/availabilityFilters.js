import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class AvailabilityFilters extends React.Component {
	render() {
		return ( 
				<div id="filter-avail" class="filter-newSec clearafter">
				    <div class="filter-newSec-head">
				        <span class="filter-newSec-head-Txt">AVAILABILITY</span>
				        <span class="filter-newSec-head-clr hide">Clear</span>
				    </div>
				    <div class="filter-newSec-rules col_2 clearafter">
				        <div id="availability1" name="availability" value="1" class="filter-newSec-rule selected">
				            <em class="filter-newSec-selct-rib"></em>
				            <p class="filter-newSec-rule-txt elipsis">Under Const.</p><p class="filter-newSec-rule-subTxt elipsis">2814</p>
				            
				        </div>
				        <div id="availability2" name="availability" value="2" class="filter-newSec-rule">
				            <em class="filter-newSec-selct-rib"></em>
				            <p class="filter-newSec-rule-txt elipsis">Ready to Move</p><p class="filter-newSec-rule-subTxt elipsis">16358</p>
				            
				        </div>
				    </div>
				    <div class="filter-newSec-rules-expand" data-action="more" data-flag="0">+ more</div>
				</div>
		);
	}
}

export default AvailabilityFilters;