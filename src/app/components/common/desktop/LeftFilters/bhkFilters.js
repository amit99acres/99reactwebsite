import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class BhkFilters extends React.Component {
	render() {
		return ( 
			<div id="filter-bhk" class="filter-newSec clearafter">
					<div class="filter-newSec-head">
						<span class="filter-newSec-head-Txt">BHK</span>
						<span class="filter-newSec-head-clr hide" >Clear</span>
					</div>
					<div class="filter-newSec-rules col_3 clearafter overflow">
						<div id="newBhk1" name="bedroom_num" value="1" class="filter-newSec-rule selected">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">1 BHK</p>
							
						</div>
						<div id="newBhk2" name="bedroom_num" value="2" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">2 BHK</p>
							
						</div>
						<div id="newBhk3" name="bedroom_num" value="3" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">3 BHK</p>
							
						</div>
						<div id="newBhk4" name="bedroom_num" value="4" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">4 BHK</p>
							
						</div>
						<div id="newBhk5" name="bedroom_num" value="5" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">5 BHK</p>
							
						</div>
						<div id="newBhk6" name="bedroom_num" value="6" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">6 BHK</p>
							
						</div>
						<div id="newBhk7" name="bedroom_num" value="7" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">7 BHK</p>
							
						</div>
						<div id="newBhk8" name="bedroom_num" value="8" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">8 BHK</p>
							
						</div>
						<div id="newBhk9" name="bedroom_num" value="9" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">9 BHK</p>
							
						</div>
						<div id="newBhk10" name="bedroom_num" value="10" class="filter-newSec-rule">
							<em class="filter-newSec-selct-rib"></em>
							<p class="filter-newSec-rule-txt elipsis">9+ BHK</p>
							
						</div>

					</div>
					<div class="filter-newSec-rules-expand" data-action="more" data-flag="0">+ more</div>
			</div>
		);
	}
}

export default BhkFilters;