import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class BudgetFilters extends React.Component {
	render() {
		return ( 
			<div class="filter-new-budget">
				<div class="filter-sub-section">
				    <div class="sub-section-head">BUDGET <div  id="lf_bud_clear" class="reset-cls hidei">Clear</div></div>
				</div>
				<div class="min-max">
				<div id="lf_budMin" class="min lf-drpdwn" >
				    <div class="minValue lf-chk"  id="lf_budget_min_label">Min</div>
				    <i class="fiter-icons-sprite filter-arrowDown lf-list"></i>
				    <div class="minmx-list  lf-minmax" >
				        <div class="lfscrollbar2" ><div class="scrollbar"><div class="track" ><div class="thumb"><div class="end"></div></div></div></div>
				            <div class="viewport " ><div class="overview" ><ul id="lf_budget_min_list"><li  class="liSel" >Min</li><li  class="" >Below 5 Lacs</li><li  class="" >5 Lacs</li><li  class="" >10 Lacs</li><li  class="" >15 Lacs</li><li  class="" >20 Lacs</li><li  class="" >25 Lacs</li><li  class="" >30 Lacs</li><li  class="" >40 Lacs</li><li  class="" >50 Lacs</li><li  class="">60 Lacs</li><li  class="" >75 Lacs</li><li  class="" >90 Lacs</li><li  class="" >1 Crore</li><li  class="" >1.5 Crores</li><li  class="" >2 Crores</li><li  class="" >3 Crores</li><li  class="" >5 Crores</li><li  class="" >10 Crores</li><li  class="" >20 Crores</li><li  class="" >30 Crores</li><li  class="" >40 Crores</li><li  class="" >50 Crores</li><li  class="" >60 Crores</li><li  class="" >70 Crores</li><li  class="" >80 Crores</li><li  class="" >90 Crores</li><li  class="" >100 Crores</li><li  class="" >100+ Crores</li></ul></div></div>
				        </div>
				    </div>
				</div>
				<div class="budgtSeperatr"></div>
				<div id="lf_budMax" class="max lf-drpdwn" >
				    <div class="maxValue lf-chk" id="lf_budget_max_label">Max</div>
				    <i class="fiter-icons-sprite filter-arrowDown lf-list"></i>
				    <div class="minmx-list  lf-minmax" >
				        <div class="lfscrollbar2" ><div class="scrollbar" ><div class="track">
				        <div class="thumb"><div class="end"></div></div></div></div>
				            <div class="viewport " ><div class="overview" ><ul id="lf_budget_max_list"><li  class="liSel" >Max</li><li  class="" >5 Lacs</li><li  class="" >10 Lacs</li><li  class="" >15 Lacs</li><li  class="" >20 Lacs</li><li  class="" >25 Lacs</li><li  class="" >30 Lacs</li><li  class="" >40 Lacs</li><li  class="" >50 Lacs</li><li  class="" >60 Lacs</li><li  class="" >75 Lacs</li><li  class="" >90 Lacs</li><li  class="" >1 Crore</li><li  class="" >1.5 Crores</li><li  class="">2 Crores</li><li  class="" >3 Crores</li><li  class="" >5 Crores</li><li  class="" >10 Crores</li><li  class="" >20 Crores</li><li  class="" >30 Crores</li><li  class="" >40 Crores</li><li  class="" >50 Crores</li><li  class="" >60 Crores</li><li  class="">70 Crores</li><li  class="" >80 Crores</li><li  class="" >90 Crores</li><li  class="" >100 Crores</li><li  class="" >100+ Crores</li><li  class="" >On Request</li></ul></div></div>
				        </div>
				    </div>
				</div>
				</div>
				</div>
		);
	}
}

export default BudgetFilters;