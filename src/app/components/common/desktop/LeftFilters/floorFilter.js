import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class FloorFilter extends React.Component {
	render() {
		return ( 
			<div>
				<div id="lfFloorSec1" class="filter-sub-section">
				    <div class="sub-section-head">Floor Number<div id="lf_floor_clear" class="reset-cls hide">Clear</div></div>
				    <i class="fiter-icons-sprite minus lfExpCol"></i>
				</div>
				<div id="lfFloorSec2" class="min-max">
					<div class="min lf-drpdwn"  id="lf_test" val="0">
					    <div class="minValue lf-chk" val="" id="lf_floor_min_label">Min</div>
					    <i class="fiter-icons-sprite filter-arrowDown"></i>				   
					 
					    <div class="minmx-list hide lf-minmax" val="0">
					        <div class="lfscrollbar2" ><div class="scrollbar"><div class="track" ><div class="thumb"><div class="end"></div></div></div></div>
					            <div class="viewport " ><div class="overview" ><ul id="lf_floor_min"><li name="floor_min" class="liSel" data-val="">Min</li><li name="floor_min" class="" data-val="-2">Basement</li><li name="floor_min" class="" data-val="-1">Lower Ground</li><li name="floor_min" class="" data-val="0">Ground Floor</li><li name="floor_min" class="" data-val="1">1</li><li name="floor_min" class="" data-val="2">2</li><li name="floor_min" class="" data-val="3">3</li><li name="floor_min" class="" data-val="4">4</li><li name="floor_min" class="" data-val="5">5</li><li name="floor_min" class="" data-val="6">6</li><li name="floor_min" class="" data-val="7">7</li><li name="floor_min" class="" data-val="8">8</li><li name="floor_min" class="" data-val="9">9</li><li name="floor_min" class="" data-val="10">10</li><li name="floor_min" class="" data-val="11">11</li><li name="floor_min" class="" data-val="12">12</li><li name="floor_min" class="" data-val="13">13</li><li name="floor_min" class="" data-val="14">14</li><li name="floor_min" class="" data-val="15">15</li><li name="floor_min" class="" data-val="16">16</li><li name="floor_min" class="" data-val="17">17</li><li name="floor_min" class="" data-val="18">18</li><li name="floor_min" class="" data-val="19">19</li><li name="floor_min" class="" data-val="20">20</li><li name="floor_min" class="" data-val="21">21</li><li name="floor_min" class="" data-val="22">22</li><li name="floor_min" class="" data-val="23">23</li><li name="floor_min" class="" data-val="24">24</li><li name="floor_min" class="" data-val="25">25</li><li name="floor_min" class="" data-val="26">26</li><li name="floor_min" class="" data-val="27">27</li><li name="floor_min" class="" data-val="28">28</li><li name="floor_min" class="" data-val="29">29</li><li name="floor_min" class="" data-val="30">30</li><li name="floor_min" class="" data-val="31">31</li><li name="floor_min" class="" data-val="32">32</li><li name="floor_min" class="" data-val="33">33</li><li name="floor_min" class="" data-val="34">34</li><li name="floor_min" class="" data-val="35">35</li><li name="floor_min" class="" data-val="36">36</li><li name="floor_min" class="" data-val="37">37</li><li name="floor_min" class="" data-val="38">38</li><li name="floor_min" class="" data-val="39">39</li><li name="floor_min" class="" data-val="40">40</li><li name="floor_min" class="" data-val="41">40+</li></ul></div></div>
					        </div>
					    </div>					    
					</div>
					<div class="hyphen filterHyphen"></div>
					<div class="max lf-drpdwn"  val="0">
					    <div class="maxValue lf-chk" val="" id="lf_floor_max_label">Max</div>
					    <i class="fiter-icons-sprite filter-arrowDown"></i>
					    <div class="minmx-list hide lf-minmax" val="0">
					        <div class="lfscrollbar2" ><div class="scrollbar" ><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
					            <div class="viewport " ><div class="overview"><ul id="lf_floor_max"><li name="floor_max" class="liSel" data-val="">Max</li><li name="floor_max" class="" data-val="-2">Basement</li><li name="floor_max" class="" data-val="-1">Lower Ground</li><li name="floor_max" class="" data-val="0">Ground Floor</li><li name="floor_max" class="" data-val="1">1</li><li name="floor_max" class="" data-val="2">2</li><li name="floor_max" class="" data-val="3">3</li><li name="floor_max" class="" data-val="4">4</li><li name="floor_max" class="" data-val="5">5</li><li name="floor_max" class="" data-val="6">6</li><li name="floor_max" class="" data-val="7">7</li><li name="floor_max" class="" data-val="8">8</li><li name="floor_max" class="" data-val="9">9</li><li name="floor_max" class="" data-val="10">10</li><li name="floor_max" class="" data-val="11">11</li><li name="floor_max" class="" data-val="12">12</li><li name="floor_max" class="" data-val="13">13</li><li name="floor_max" class="" data-val="14">14</li><li name="floor_max" class="" data-val="15">15</li><li name="floor_max" class="" data-val="16">16</li><li name="floor_max" class="" data-val="17">17</li><li name="floor_max" class="" data-val="18">18</li><li name="floor_max" class="" data-val="19">19</li><li name="floor_max" class="" data-val="20">20</li><li name="floor_max" class="" data-val="21">21</li><li name="floor_max" class="" data-val="22">22</li><li name="floor_max" class="" data-val="23">23</li><li name="floor_max" class="" data-val="24">24</li><li name="floor_max" class="" data-val="25">25</li><li name="floor_max" class="" data-val="26">26</li><li name="floor_max" class="" data-val="27">27</li><li name="floor_max" class="" data-val="28">28</li><li name="floor_max" class="" data-val="29">29</li><li name="floor_max" class="" data-val="30">30</li><li name="floor_max" class="" data-val="31">31</li><li name="floor_max" class="" data-val="32">32</li><li name="floor_max" class="" data-val="33">33</li><li name="floor_max" class="" data-val="34">34</li><li name="floor_max" class="" data-val="35">35</li><li name="floor_max" class="" data-val="36">36</li><li name="floor_max" class="" data-val="37">37</li><li name="floor_max" class="" data-val="38">38</li><li name="floor_max" class="" data-val="39">39</li><li name="floor_max" class="" data-val="40">40</li><li name="floor_max" class="" data-val="41">40+</li></ul></div></div>
					        </div>
					    </div>
					</div>
					</div>
			</div>
		);
	}
}

export default FloorFilter;