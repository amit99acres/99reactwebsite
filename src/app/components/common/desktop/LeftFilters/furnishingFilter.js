import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class FurnishingFilter extends React.Component {
	render() {
		return ( 
			<div>
				<div id="lfFurSec1" class="filter-sub-section">
				    <div class="sub-section-head">Furnishing<div id="furnish_clear" class="reset-cls chk-clear hide">Clear</div></div>
				    <i class="fiter-icons-sprite minus lfExpCol"></i>
				</div>
				<div id="lfFurSec2" class="lf-ul-cnt">
				   <ul class="html-checkBox colm2" id="lf_furnishList">
				      <li><label for="furnish_1"><input name="furnish" type="checkbox" value="1" id="furnish_1" /><span></span>Furnished</label></li>
				      <li><label for="furnish_2"><input name="furnish" type="checkbox" value="2" id="furnish_2" /><span></span>Unfurnished</label></li>
				      <li><label for="furnish_4"><input name="furnish" type="checkbox" value="4" id="furnish_4" /><span></span>Semifurnished</label></li>
				   </ul>
				</div>
			</div>
		);
	}
}

export default FurnishingFilter;