import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';
import SortBy from './sortBy';
import BhkFilters from './bhkFilters';
import BudgetFilters from './budgetFilters';
import AvailabilityFilters from './availabilityFilters';
import SaleType from './saleType';
import ResidentialType from './residentialType';
import LocalityFilter from './localityFilter';
import SocietyFilter from './societyFilter';
import VerifiedListings from './verifiedListings';
import PhotosFilter from './photosFilter';
import VideosFilter from './videosFilter';
import AmenitiesFilter from './amenitiesFilter';
import PostedByFilter from './postedByFilter';
import FurnishingFilter from './furnishingFilter';
import AreaFilter from './areaFilter';
import FloorFilter from './floorFilter';


import './component.css';

class Header extends React.Component {
	  constructor(props) {
    super(props);
}

	render() {
		const {leftFiltrsticky} = this.props; 	
		const {fixcomp} = this.props; 	
		const {stickyHg}  = this.props;
		const {btmTopShift} = this.props;
		
	    const filterStyle = {
       		 height: this.props.leftFiltrsticky == 'True' ? this.props.stickyHg : '', 
       		 top: this.props.leftFiltrsticky == 'True' ? this.props.fixcomp : this.props.btmTopShift, 
   		 };
	  
	    var clsName = this.props.leftFiltrsticky == 'True' ? 'filterStcky' : ''; 

		return ( 
			<div  id="LEFT_FILTER_BOX" class={`lSide-srp-filters boxSize abs ${clsName}`} style={filterStyle} >
				<SortBy />
				<div class="filter-sections">
			        <div class="filter-head">
			            <div class="head130">FILTER</div>
					</div>
				</div>
				<div class="filter-newSec-Wrpr">
					<BhkFilters />
					<BudgetFilters />
					<AvailabilityFilters />
					<SaleType />
					<ResidentialType />
				</div>
				<div class="mt20 mrfHeadTxt">
			        <span class="">MORE FILTERS</span>
			    </div>
			    <div class="filter-body mt10">
			    	<LocalityFilter />
			    	<div class="hR_Line"></div>
			    	<SocietyFilter />
			    	<div class="hR_Line"></div>
			    	<VerifiedListings />
			    	<PhotosFilter />
			    	<VideosFilter />
			    	<div class="hR_Line"></div>
			    	<AmenitiesFilter />
			    	<div class="hR_Line"></div>
			    	<PostedByFilter />
			    	<div class="hR_Line"></div>
			    	<FurnishingFilter />
			    	<div class="hR_Line"></div>
			    	<AreaFilter />
			    	<div class="hR_Line"></div>
			    	<FloorFilter />
			    </div>
			</div>
		);
	}
}

export default Header;