import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class LocalityFilter extends React.Component {
	render() {
		return ( 
			<div>
				<div class="filter-sub-section lf_loc_context">
				    <div class="sub-section-head">Locality<div id="lfLoc_clear" class="reset-cls chk-clear hide">Clear</div></div>
				    <i class="fiter-icons-sprite lfExpCol minus"></i>
				</div>
				<div class="lf-ul-cnt" id="lf_locality" >
					<div class="filter-loc-search-box">
					    <i class="fiter-icons-sprite filter-loc-srch-icn"></i>
					    <input id="lf_loc_keyword" type="text" />
					    <i class="bb-cross iconS hide"></i>
					</div>
					<div class="lfscrollbar2 mb10" ><div class="scrollbar" ><div class="track" ><div class="thumb" ><div class="end"></div></div></div></div>
					    <div class="viewport " >
					    <div class="overview">
<ul class="html-checkBox colm2" id="lf_loc_list">
   <li class="loc-heading _lfLocMoreHeading">Top Localities</li>
   <li class="lfLocRow"><label for="lfLoc_8232"><input type="checkbox"   value="8232" id="lfLoc_8232" />
   <span></span><span class="lflblTxt">Noida Extension</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7858"><input type="checkbox"   value="7858" id="lfLoc_7858" />
   <span></span><span class="lflblTxt">Sector-137 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8495"><input type="checkbox"   value="8495" id="lfLoc_8495" />
   <span></span><span class="lflblTxt">Sector-150 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8398"><input type="checkbox"   value="8398" id="lfLoc_8398" />
   <span></span><span class="lflblTxt">Sector-78 Noida</span></label></li>
   <li class="loc-heading _lfLocMoreHeading">More Localities</li>
   <li class="lfLocRow"><label for="lfLoc_7837">
      <input type="checkbox"   value="7837" id="lfLoc_7837" />
      <span></span><span class="lflblTxt">sector-121 Noida</span></label>
   </li>
   <li class="lfLocRow"><label for="lfLoc_6166"><input type="checkbox"   value="6166" id="lfLoc_6166" />
   <span></span><span class="lflblTxt">Sector-128 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8529"><input type="checkbox"   value="8529" id="lfLoc_8529" />
   <span></span><span class="lflblTxt">Sector-168 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7808"><input type="checkbox"   value="7808" id="lfLoc_7808" />
   <span></span><span class="lflblTxt">Sector-134 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8511"><input type="checkbox"   value="8511" id="lfLoc_8511" />
   <span></span><span class="lflblTxt">Sector-74 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8475"><input type="checkbox"   value="8475" id="lfLoc_8475" />
   <span></span><span class="lflblTxt">Sector-143 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_162"><input type="checkbox"   value="162" id="lfLoc_162" />
   <span></span><span class="lflblTxt">Sector-107 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7867"><input type="checkbox"   value="7867" id="lfLoc_7867" />
   <span></span><span class="lflblTxt">Sector-75 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_99"><input type="checkbox"   value="99" id="lfLoc_99" />
   <span></span><span class="lflblTxt">Sector-45 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7804"><input type="checkbox"   value="7804" id="lfLoc_7804" />
   <span></span><span class="lflblTxt">Sector-100 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_11014"><input type="checkbox"   value="11014" id="lfLoc_11014" />
   <span></span><span class="lflblTxt">Sector-93 A Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_79"><input type="checkbox"   value="79" id="lfLoc_79" />
   <span></span><span class="lflblTxt">Sector-50 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8468"><input type="checkbox"   value="8468" id="lfLoc_8468" />
   <span></span><span class="lflblTxt">Sector-77 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_163"><input type="checkbox"   value="163" id="lfLoc_163" />
   <span></span><span class="lflblTxt">Sector-110 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_4836"><input type="checkbox"   value="4836" id="lfLoc_4836" />
   <span></span><span class="lflblTxt">Sector-135 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7844"><input type="checkbox"   value="7844" id="lfLoc_7844" />
   <span></span><span class="lflblTxt">Sector-133 Noida </span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7876"><input type="checkbox"   value="7876" id="lfLoc_7876" />
   <span></span><span class="lflblTxt">Sector-76 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_153"><input type="checkbox"   value="153" id="lfLoc_153" />
   <span></span><span class="lflblTxt">Sector-82 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7878"><input type="checkbox"   value="7878" id="lfLoc_7878" />
   <span></span><span class="lflblTxt">Sector-104 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_95"><input type="checkbox"   value="95" id="lfLoc_95" />
   <span></span><span class="lflblTxt">Sector-93 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_105"><input type="checkbox"   value="105" id="lfLoc_105" />
   <span></span><span class="lflblTxt">Sector-44 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_11016"><input type="checkbox"   value="11016" id="lfLoc_11016" />
   <span></span><span class="lflblTxt">Sector-93 B Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7779"><input type="checkbox"   value="7779" id="lfLoc_7779" />
   <span></span><span class="lflblTxt">Sector-129 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7850"><input type="checkbox"   value="7850" id="lfLoc_7850" />
   <span></span><span class="lflblTxt">Sector-120 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_9158"><input type="checkbox"   value="9158" id="lfLoc_9158" />
   <span></span><span class="lflblTxt">Sector-79 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_34431"><input type="checkbox"   value="34431" id="lfLoc_34431" />
   <span></span><span class="lflblTxt">Chhalera</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7826"><input type="checkbox"   value="7826" id="lfLoc_7826" />
   <span></span><span class="lflblTxt">Sector-70 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_4297"><input type="checkbox"   value="4297" id="lfLoc_4297" />
   <span></span><span class="lflblTxt">Sector-73 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8097"><input type="checkbox"   value="8097" id="lfLoc_8097" />
   <span></span><span class="lflblTxt">Sector-119 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_76"><input type="checkbox"   value="76" id="lfLoc_76" />
   <span></span><span class="lflblTxt">Sector-47 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7787"><input type="checkbox"   value="7787" id="lfLoc_7787" />
   <span></span><span class="lflblTxt">Sector-151 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7425"><input type="checkbox"   value="7425" id="lfLoc_7425" />
   <span></span><span class="lflblTxt">Sector-15A Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_139"><input type="checkbox"   value="139" id="lfLoc_139" />
   <span></span><span class="lflblTxt">Sector-71 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_89"><input type="checkbox"   value="89" id="lfLoc_89" />
   <span></span><span class="lflblTxt">Sector-61 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_94"><input type="checkbox"   value="94" id="lfLoc_94" />
   <span></span><span class="lflblTxt">Sector-46 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_101"><input type="checkbox"   value="101" id="lfLoc_101" />
   <span></span><span class="lflblTxt">Sector-49 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_160"><input type="checkbox"   value="160" id="lfLoc_160" />
      <span></span><span class="lflblTxt">Sector-105 Noida</span></label>
   </li>
   <li class="lfLocRow"><label for="lfLoc_8883"><input type="checkbox"   value="8883" id="lfLoc_8883" />
   <span></span><span class="lflblTxt">Sector-145 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_164"><input type="checkbox"   value="164" id="lfLoc_164" />
   <span></span><span class="lflblTxt">Sector-122 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_90"><input type="checkbox"   value="90" id="lfLoc_90" />
   <span></span><span class="lflblTxt">Sector-62 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_75"><input type="checkbox"   value="75" id="lfLoc_75" />
   <span></span><span class="lflblTxt">Sector-29 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_161"><input type="checkbox"   value="161" id="lfLoc_161" />
   <span></span><span class="lflblTxt">Sector-108 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_126"><input type="checkbox"   value="126" id="lfLoc_126" />
   <span></span><span class="lflblTxt">Sector-34 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_46408"><input type="checkbox"   value="46408" id="lfLoc_46408" />
   <span></span><span class="lflblTxt">Sector 63A Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_80"><input type="checkbox"   value="80" id="lfLoc_80" />
   <span></span><span class="lflblTxt">Sector-51 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_93"><input type="checkbox"   value="93" id="lfLoc_93" />
   <span></span><span class="lflblTxt">Sector-92 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_13216"><input type="checkbox"   value="13216" id="lfLoc_13216" />
   <span></span><span class="lflblTxt">Sector-152 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_72"><input type="checkbox"   value="72" id="lfLoc_72" />
   <span></span><span class="lflblTxt">Sector-41 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_77"><input type="checkbox"   value="77" id="lfLoc_77" />
   <span></span><span class="lflblTxt">Sector-39 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_98"><input type="checkbox"   value="98" id="lfLoc_98" />
   <span></span><span class="lflblTxt">Sector-52 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_128"><input type="checkbox"   value="128" id="lfLoc_128" />
   <span></span><span class="lflblTxt">Sector-37 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_120"><input type="checkbox"   value="120" id="lfLoc_120" />
   <span></span><span class="lflblTxt">Sector-26 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_104"><input type="checkbox"   value="104" id="lfLoc_104" />
   <span></span><span class="lflblTxt">Sector-40 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_122"><input type="checkbox"   value="122" id="lfLoc_122" />
   <span></span><span class="lflblTxt">Sector-30 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_13260"><input type="checkbox"   value="13260" id="lfLoc_13260" />
   <span></span><span class="lflblTxt">Hajipur</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_74"><input type="checkbox"   value="74" id="lfLoc_74" />
   <span></span><span class="lflblTxt">Sector-25 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8476"><input type="checkbox"   value="8476" id="lfLoc_8476" />
   <span></span><span class="lflblTxt">Sector-144 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_73"><input type="checkbox"   value="73" id="lfLoc_73" />
   <span></span><span class="lflblTxt">Sector-36 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_10838"><input type="checkbox"   value="10838" id="lfLoc_10838" />
   <span></span><span class="lflblTxt">Block C Sector-44 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_8477"><input type="checkbox"   value="8477" id="lfLoc_8477" />
   <span></span><span class="lflblTxt">Sector-118 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7799"><input type="checkbox"   value="7799" id="lfLoc_7799" />
   <span></span><span class="lflblTxt">Sector-117 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_13206"><input type="checkbox"   value="13206" id="lfLoc_13206" />
   <span></span><span class="lflblTxt">Sector-143B Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_115"><input type="checkbox"   value="115" id="lfLoc_115" />
   <span></span><span class="lflblTxt">Sector-21 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_100"><input type="checkbox"   value="100" id="lfLoc_100" />
   <span></span><span class="lflblTxt">Sector-48 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_140"><input type="checkbox"   value="140" id="lfLoc_140" />
   <span></span><span class="lflblTxt">Sector-72 Noida</span></label></li>
   <li class="lfLocRow"><label for="lfLoc_7785"><input type="checkbox"   value="7785" id="lfLoc_7785" />
   <span></span><span class="lflblTxt">Sector-99 Noida</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_121"><input type="checkbox"   value="121" id="lfLoc_121" />
   <span></span><span class="lflblTxt">Sector-28 Noida</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_81"><input type="checkbox"   value="81" id="lfLoc_81" />
   <span></span><span class="lflblTxt">Sector-53 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_113"><input type="checkbox"   value="113" id="lfLoc_113" />
   <span></span><span class="lflblTxt">Sector-19 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_10274"><input type="checkbox"   value="10274" id="lfLoc_10274" />
   <span></span><span class="lflblTxt">Sector-116 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_108"><input type="checkbox"   value="108" id="lfLoc_108" />
   <span></span><span class="lflblTxt">Sector-12 Noida</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_109"><input type="checkbox"   value="109" id="lfLoc_109" />
   <span></span><span class="lflblTxt">Sector-14 Noida</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_13214"><input type="checkbox"   value="13214" id="lfLoc_13214" />
   <span></span><span class="lflblTxt">Sector-149 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_123"><input type="checkbox"   value="123" id="lfLoc_123" />
   <span></span><span class="lflblTxt">Sector-31 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_10272"><input type="checkbox"   value="10272" id="lfLoc_10272" />
   <span></span><span class="lflblTxt">Sector-115 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_152"><input type="checkbox"   value="152" id="lfLoc_152" />
   <span></span><span class="lflblTxt">Sector-81 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_78"><input type="checkbox"   value="78" id="lfLoc_78" />
   <span></span><span class="lflblTxt">Sector-27 Noida</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_10840"><input type="checkbox"   value="10840" id="lfLoc_10840" />
   <span></span><span class="lflblTxt">Block G Sector-44 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_13256"><input type="checkbox"   value="13256" id="lfLoc_13256" />
   <span></span><span class="lflblTxt">Baraula</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_4839"><input type="checkbox"   value="4839" id="lfLoc_4839" />
   <span></span><span class="lflblTxt">Sector-98 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_7860"><input type="checkbox"   value="7860" id="lfLoc_7860" />
   <span></span><span class="lflblTxt">Sector-131 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_21403"><input type="checkbox"   value="21403" id="lfLoc_21403" />
   <span></span><span class="lflblTxt">Khora Colony</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_83"><input type="checkbox"   value="83" id="lfLoc_83" />
   <span></span><span class="lflblTxt">Sector-55 Noida</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_44930"><input type="checkbox"   value="44930" id="lfLoc_44930" />
   <span></span><span class="lflblTxt">Arun Vihar</span></label></li>
   <li class="lfLocRow hidei" ><label for="lfLoc_117"><input type="checkbox"   value="117" id="lfLoc_117" />
   <span></span><span class="lflblTxt">Sector-22 Noida</span></label></li>
   <li class="lfLocRow hidei"><label for="lfLoc_8094"><input type="checkbox"   value="8094" id="lfLoc_8094" />
   <span></span><span class="lflblTxt">Noida-Greater Noida Expressway</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_107"><input type="checkbox"   value="107" id="lfLoc_107" />
   <span></span><span class="lflblTxt">Sector-11 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_110"><input type="checkbox"   value="110" id="lfLoc_110" />
   <span></span><span class="lflblTxt">Sector-15 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_10830"><input type="checkbox"   value="10830" id="lfLoc_10830" />
   <span></span><span class="lflblTxt">Block B Sector-50 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_125"><input type="checkbox"   value="125" id="lfLoc_125" />
   <span></span><span class="lflblTxt">Sector-33 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_10842"><input type="checkbox"   value="10842" id="lfLoc_10842" />
   <span></span><span class="lflblTxt">Block B Sector-44 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_84"><input type="checkbox"   value="84" id="lfLoc_84" />
   <span></span><span class="lflblTxt">Sector-56 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_7782"><input type="checkbox"   value="7782" id="lfLoc_7782" />
   <span></span><span class="lflblTxt">Sector-124 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_10832"><input type="checkbox"   value="10832" id="lfLoc_10832" />
   <span></span><span class="lflblTxt">Block D Sector-50 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_114">
      <input type="checkbox"   value="114" id="lfLoc_114" />
      <span></span><span class="lflblTxt">Sector-20 Noida</span></label>
   </li>
   <li class="lfLocRow"   ><label for="lfLoc_118"><input type="checkbox"   value="118" id="lfLoc_118" />
   <span></span><span class="lflblTxt">Sector-23 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13194"><input type="checkbox"   value="13194" id="lfLoc_13194" />
   <span></span><span class="lflblTxt">Sector-123 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_8093"><input type="checkbox"   value="8093" id="lfLoc_8093" />
   <span></span><span class="lflblTxt">Sector-19A Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_167"><input type="checkbox"   value="167" id="lfLoc_167" />
   <span></span><span class="lflblTxt">Sector-126 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_112"><input type="checkbox"   value="112" id="lfLoc_112" />
   <span></span><span class="lflblTxt">Sector-17 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_10834"><input type="checkbox"   value="10834" id="lfLoc_10834" />
   <span></span><span class="lflblTxt">Block E Sector-50 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13228"><input type="checkbox"   value="13228" id="lfLoc_13228" />
   <span></span><span class="lflblTxt">Sector-16B Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13266"><input type="checkbox"   value="13266" id="lfLoc_13266" />
   <span></span><span class="lflblTxt">Parthala Khanjarpur</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_8332"><input type="checkbox"   value="8332" id="lfLoc_8332" />
   <span></span><span class="lflblTxt">Sector-94 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_91"><input type="checkbox"   value="91" id="lfLoc_91" />
   <span></span><span class="lflblTxt">Sector-63 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_102"><input type="checkbox"   value="102" id="lfLoc_102" />
   <span></span><span class="lflblTxt">Sector-42 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_4806"><input type="checkbox"   value="4806" id="lfLoc_4806" />
   <span></span><span class="lflblTxt">Sector-88 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_16682"><input type="checkbox"   value="16682" id="lfLoc_16682" />
   <span></span><span class="lflblTxt">Sarfabad Village</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_151"><input type="checkbox"   value="151" id="lfLoc_151" />
   <span></span><span class="lflblTxt">Sector-80 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_165"><input type="checkbox"   value="165" id="lfLoc_165" />
   <span></span><span class="lflblTxt">Sector-112 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_4807"><input type="checkbox"   value="4807" id="lfLoc_4807" />
   <span></span><span class="lflblTxt">Sector-132 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_46519"><input type="checkbox"   value="46519" id="lfLoc_46519" />
   <span></span><span class="lflblTxt">Basai</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_124"><input type="checkbox"   value="124" id="lfLoc_124" />
   <span></span><span class="lflblTxt">Sector-32 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13196"><input type="checkbox"   value="13196" id="lfLoc_13196" />
   <span></span><span class="lflblTxt">Sector-130 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_127"><input type="checkbox"   value="127" id="lfLoc_127" />
   <span></span><span class="lflblTxt">Sector-35 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_141"><input type="checkbox"   value="141" id="lfLoc_141" />
   <span></span><span class="lflblTxt">Sector-43 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13254"><input type="checkbox"   value="13254" id="lfLoc_13254" />
   <span></span><span class="lflblTxt">Sorkha</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_34931"><input type="checkbox"   value="34931" id="lfLoc_34931" />
   <span></span><span class="lflblTxt">Salarpur</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_92"><input type="checkbox"   value="92" id="lfLoc_92" />
   <span></span><span class="lflblTxt">Sector-18 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_6163"><input type="checkbox"   value="6163" id="lfLoc_6163" />
   <span></span><span class="lflblTxt">Sector-142 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_8527"><input type="checkbox"   value="8527" id="lfLoc_8527" />
   <span></span><span class="lflblTxt">Sector-113 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_41121"><input type="checkbox"   value="41121" id="lfLoc_41121" />
   <span></span><span class="lflblTxt">Film City</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_7503"><input type="checkbox"   value="7503" id="lfLoc_7503" />
   <span></span><span class="lflblTxt">Sector-68 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13212"><input type="checkbox"   value="13212" id="lfLoc_13212" />
   <span></span><span class="lflblTxt">Sector-148 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13234"><input type="checkbox"   value="13234" id="lfLoc_13234" />
   <span></span><span class="lflblTxt">Sector-163 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13248"><input type="checkbox"   value="13248" id="lfLoc_13248" />
   <span></span><span class="lflblTxt">Sector-89 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_4831"><input type="checkbox"   value="4831" id="lfLoc_4831" />
   <span></span><span class="lflblTxt">Bhangel</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_4837"><input type="checkbox"   value="4837" id="lfLoc_4837" />
   <span></span><span class="lflblTxt">Sector-66 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_10836"><input type="checkbox"   value="10836" id="lfLoc_10836" />
   <span></span><span class="lflblTxt">Block F Sector-50 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_11064"><input type="checkbox"   value="11064" id="lfLoc_11064" />
   <span></span><span class="lflblTxt">Block H Sector 44 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13198"><input type="checkbox"   value="13198" id="lfLoc_13198" />
   <span></span><span class="lflblTxt">Sector-14A Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_41919"><input type="checkbox"   value="41919" id="lfLoc_41919" />
   <span></span><span class="lflblTxt">Garhi Chaukhandi</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_96"><input type="checkbox"   value="96" id="lfLoc_96" />
   <span></span><span class="lflblTxt">Sector-64 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_7502"><input type="checkbox"   value="7502" id="lfLoc_7502" />
   <span></span><span class="lflblTxt">Sector-96 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_9032"><input type="checkbox"   value="9032" id="lfLoc_9032" />
   <span></span><span class="lflblTxt">Sector-162 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13188"><input type="checkbox"   value="13188" id="lfLoc_13188" />
   <span></span><span class="lflblTxt">Chaukhandi</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13202"><input type="checkbox"   value="13202" id="lfLoc_13202" />
   <span></span><span class="lflblTxt">Sector-141 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13204"><input type="checkbox"   value="13204" id="lfLoc_13204" />
   <span></span><span class="lflblTxt">Sector-143A Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13246"><input type="checkbox"   value="13246" id="lfLoc_13246" />
   <span></span><span class="lflblTxt">Sector-87 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_46718"><input type="checkbox"   value="46718" id="lfLoc_46718" />
   <span></span><span class="lflblTxt">Chotpur Colony</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_47412"><input type="checkbox"   value="47412" id="lfLoc_47412" />
   <span></span><span class="lflblTxt">Nagli Bahrampur</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_82"><input type="checkbox"   value="82" id="lfLoc_82" />
   <span></span><span class="lflblTxt">Sector-54 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_85"><input type="checkbox"   value="85" id="lfLoc_85" />
   <span></span><span class="lflblTxt">Sector-57 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_86"><input type="checkbox"   value="86" id="lfLoc_86" />
   <span></span><span class="lflblTxt">Sector-58 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_87"><input type="checkbox"   value="87" id="lfLoc_87" />
   <span></span><span class="lflblTxt">Sector-59 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_88"><input type="checkbox"   value="88" id="lfLoc_88" />
   <span></span><span class="lflblTxt">Sector-60 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_97"><input type="checkbox"   value="97" id="lfLoc_97" />
   <span></span><span class="lflblTxt">Sector-65 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_145"><input type="checkbox"   value="145" id="lfLoc_145" />
   <span></span><span class="lflblTxt">Sector-4 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_146"><input type="checkbox"   value="146" id="lfLoc_146" />
   <span></span><span class="lflblTxt">Sector-5 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_166"><input type="checkbox"   value="166" id="lfLoc_166" />
   <span></span><span class="lflblTxt">Sector-125 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_168"><input type="checkbox"   value="168" id="lfLoc_168" />
   <span></span><span class="lflblTxt">Sector-127 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_4833"><input type="checkbox"   value="4833" id="lfLoc_4833" />
   <span></span><span class="lflblTxt">Sector-67 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_5444"><input type="checkbox"   value="5444" id="lfLoc_5444" />
   <span></span><span class="lflblTxt">Phase-II</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_10350"><input type="checkbox"   value="10350" id="lfLoc_10350" />
   <span></span><span class="lflblTxt">Sector-154 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13186"><input type="checkbox"   value="13186" id="lfLoc_13186" />
   <span></span><span class="lflblTxt">Bahlolpur</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13192"><input type="checkbox"   value="13192" id="lfLoc_13192" />
   <span></span><span class="lflblTxt">Sector-102 Noida</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_13262"><input type="checkbox"   value="13262" id="lfLoc_13262" />
   <span></span><span class="lflblTxt">Mamura</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_29649"><input type="checkbox"   value="29649" id="lfLoc_29649" />
   <span></span><span class="lflblTxt">Suthiyana</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_32821"><input type="checkbox"   value="32821" id="lfLoc_32821" />
   <span></span><span class="lflblTxt">Chhajarsi Colony</span></label></li>
   <li class="lfLocRow"   ><label for="lfLoc_39830"><input type="checkbox"   value="39830" id="lfLoc_39830" />
   <span></span><span class="lflblTxt">Rasoolpur Nawada</span></label></li>
</ul>
</div></div></div>
					</div>
			</div>
		);
	}
}

export default LocalityFilter;