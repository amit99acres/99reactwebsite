import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class PhotosFilter extends React.Component {
	render() {
		return ( 
			
				<div class="filter-head propListQuality cPin ">
				<div class="html-checkBox">
				    <label for="lf_photos">
				        <input type="checkbox" name="media" id="lf_photos" value="PHOTO" />
				        <span></span><span id="lf_photo_span" class="cPin">With Photos</span>
				    </label>
				</div>
				</div>
			
		);
	}
}

export default PhotosFilter;