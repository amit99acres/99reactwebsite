import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class PostedByFilter extends React.Component {
	render() {
		return ( 
			<div>
				<div class="filter-sub-section">
				    <div class="sub-section-head">Posted By<div id="lf_posted_by_clear" class="reset-cls chk-clear hide">Clear</div></div>
				    <i class="fiter-icons-sprite minus lfExpCol"></i>
				</div>
				<div class="lf-ul-cnt">
				   <ul class="html-checkBox colm2" id="lf_PostedByList">
				      <li><label for="postedBy_O"><input name="class" type="checkbox" value="O" id="postedBy_O" /><span></span>Owner</label></li>
				      <li><label for="postedBy_B"><input name="class" type="checkbox" value="B" id="postedBy_B" /><span></span>Builder</label></li>
				      <li><label for="postedBy_A"><input name="class" type="checkbox" value="A" id="postedBy_A" /><span></span>Dealer</label></li>
				   </ul>
				</div>
			</div>
		);
	}
}

export default PostedByFilter;