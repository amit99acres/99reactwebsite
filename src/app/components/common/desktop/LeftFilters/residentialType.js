import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class ResidentialType extends React.Component {
	render() {
		return ( 
			<div id="filter-resident" class="filter-newSec clearafter">
			    <div class="filter-newSec-head">
			        <span class="filter-newSec-head-Txt">RESIDENTIAL TYPE</span>
			        <span class="filter-newSec-head-clr hide">Clear</span>
			    </div>
			    <div class="filter-newSec-rules col_2 clearafter">
			        <div id="resType1" name="property_type" value="1" class="filter-newSec-rule selected">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Apartments</p><p class="filter-newSec-rule-subTxt elipsis">14688</p>
			            
			        </div>
			        <div id="resType4" name="property_type" value="4" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Builder Floor</p><p class="filter-newSec-rule-subTxt elipsis">344</p>
			            
			        </div>
			        <div id="resType2" name="property_type" value="2" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Villa</p><p class="filter-newSec-rule-subTxt elipsis">1470</p>
			            
			        </div>
			        <div id="resType3" name="property_type" value="3" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Land</p><p class="filter-newSec-rule-subTxt elipsis">2345</p>
			            
			        </div>
			        <div id="resType5" name="property_type" value="5" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Farm House</p><p class="filter-newSec-rule-subTxt elipsis">223</p>
			            
			        </div>
			        <div id="resType90" name="property_type" value="90" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Studio Apartment</p><p class="filter-newSec-rule-subTxt elipsis">89</p>
			            
			        </div>
			        <div id="resType22" name="property_type" value="22" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Serviced Apartments</p><p class="filter-newSec-rule-subTxt elipsis">11</p>
			            
			        </div>

			    </div>
			    <div class="filter-newSec-rules-expand" data-action="more" data-flag="0">+ more</div>
			</div>
		);
	}
}

export default ResidentialType;