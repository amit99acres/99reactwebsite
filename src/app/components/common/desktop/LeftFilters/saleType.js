import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class saleType extends React.Component {
	render() {
		return ( 
			<div id="filter-sale" class="filter-newSec clearafter">
			    <div class="filter-newSec-head">
			        <span class="filter-newSec-head-Txt">SALE TYPE</span>
			        <span class="filter-newSec-head-clr hide">Clear</span>
			    </div>
			    <div class="filter-newSec-rules col_2 clearafter">
			        <div id="saletype1" name="transact_type" value="1" class="filter-newSec-rule selected">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">Resale</p><p class="filter-newSec-rule-subTxt elipsis">16224</p>
			            
			        </div>
			        <div id="saletype2" name="transact_type" value="2" class="filter-newSec-rule">
			            <em class="filter-newSec-selct-rib"></em>
			            <p class="filter-newSec-rule-txt elipsis">New booking</p><p class="filter-newSec-rule-subTxt elipsis">3285</p>
			            
			        </div>
			    </div>
			    <div class="filter-newSec-rules-expand" data-action="more" data-flag="0">+ more</div>
			</div>
		);
	}
}

export default saleType;