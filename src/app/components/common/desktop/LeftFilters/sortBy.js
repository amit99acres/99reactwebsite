import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class SortBy extends React.Component {
	render() {
		return ( 
			<div>
				<div class="filter-sections" id="filter-sort-by">
				    <div class="filter-head cPin" id="lf-sort-by">
				        <div>SORT BY</div>
				        <div id="lf_sortByParam">(Relevance)</div>
				        <i class="fiter-icons-sprite filter-arrowDown"></i>
				    </div>
				    <div id="lf_sort_by_cnt" class="filter-body sortBy " >
				        <ul id="lf_sortBy_lst" class="html-checkBox">
				            <li class="sel">
				                <span class="hide">rel</span>
				                <label for="spm1">
				                    Relevance
				                </label>
				            </li>
				            <li>
				                <span class="hide">date_d</span>
				                <label for="spm2">
				                    Date: Newest
				                </label>
				            </li>
				            <li>
				                <span class="hide">price_a</span>
				                <label for="spm3">
				                    Price: Low-High
				                </label>
				            </li>
				            <li>
				                <span class="hide">price_d</span>
				                <label for="spm4">
				                    Price: High-Low
				                </label>
				            </li>
				            <li id="lf_rate_sqft_a hide" >
				                <span class="hide">rate_sqft_a</span>
				                <label for="spm4">
				                    Rate/Unit: Low
				                </label>
				            </li>
				            <li id="lf_rate_sqft_d hide">
				                <span class="hide">rate_sqft_d</span>
				                <label for="spm4">
				                    Rate/Unit: High
				                </label>
				            </li>
				        </ul>
				    </div>
				</div>
			</div>
		);
	}
}

export default SortBy;