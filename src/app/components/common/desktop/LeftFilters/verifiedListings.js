import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class verifiedListings extends React.Component {
	render() {
		return ( 
			
				<div class="filter-head propListQuality cPin">
				    <div class="html-checkBox">
				        <label for="lf_verified_listing">
				            <input type="checkbox" name="verified" id="lf_verified_listing" value="Y" />
				            <span></span><span id="lf_ver_span" class="cPin">Verified Listing</span>
				        </label>
				    </div>
				</div>
			
		);
	}
}

export default verifiedListings;