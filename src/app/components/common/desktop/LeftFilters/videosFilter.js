import React          from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

import './component.css';

class VideosFilter extends React.Component {
	render() {
		return ( 
			
				<div class="filter-head propListQuality bdr0 cPin">
				<div class="html-checkBox">
				    <label for="lf_videos">
				        <input type="checkbox" name="media" id="lf_videos" value="VIDEO" />
				        <span></span><span id="lf_video_span" class="cPin">With Videos</span>
				    </label>
				</div>
				</div>
			
		);
	}
}

export default VideosFilter;