import React          from 'react'; 
import './component.css';

class SendFeedback extends React.Component {
	constructor(props) {
	  super(props);
	  	this.state = {
		  showFeedBck : false
	    };
	    this.flag = true;
	  this.showfeedBackForm = this.showfeedBackForm.bind(this);
	  this.closefeedbck = this.closefeedbck.bind(this);
	}

	showfeedBackForm =(e) =>{	
		var active = true;
    	this.setState({ showFeedBck: active });
    	let bgLyr = this.flag ? "on" : "off";
	    this.props.onBgOn(bgLyr);
	}
	closefeedbck =(e) =>{	
		var active = false;
    	this.setState({ showFeedBck: active });
    	let bgLyr = "off";
	    this.props.onBgOn(bgLyr);
	}

	render() {
		var clsName = this.state.showFeedBck == true ? 'dispBlck' : ''; 
		return ( 
			<div>
				<div className="feedback_widget iconS" onClick={this.showfeedBackForm}></div>
				<div className={`feedback_wdgtForm ${clsName}`}>
					<form class="hide">
						<div className="formHeadin">Please share your feedback for this page.</div>

						<label><span className="red">* </span> Email:&nbsp;</label>
						<div >
							<input type="text" name="Email" id=""  required="true" />
							</div>						
						<label ><span className="red">* </span> Feedback:&nbsp;</label>
						<div>
						<textarea placeholder="My idea for this page is...."  required="true" valtype="msg" ></textarea>
						</div>
						<input type="hidden" value="SEARCH" name="page_src" id="page_src" />
						<div className="clr pdt7"></div>
						 <div id="submit_SES">
						 <label>&nbsp;</label>
						        <input type="submit" value="Send" className="btn blue f12" />
						      <a href="" className="uline f12">Cancel</a>
						    </div>
					
				  </form>
				  <div>
				  	<div class="feedbckThnkWrp">
				  		<div class="spacer10"></div>
						<div class="okayIcnGreen"></div>
						<div class="feedBckThankTxt">
							<span>Thank You! </span>
							 We appreciate your feedback
						</div>
						<div class="spacer10"></div>
						<input type="submit" value="OK" className="btn blue f12" />
						<div class="spacer10"></div>
					</div>
				  </div>
				  	<i className="iconS md_cross" onClick={this.closefeedbck}></i>
				</div>
				
			</div>
		);
	}
}

export default SendFeedback;