import React          from 'react'; 
import './component.css';

class TopArrow extends React.Component {
constructor(props) {
	  super(props);
	   this.state={	
	        intervalId: 0
	    }
	this.scrollStep = this.scrollStep.bind(this);
	this.scrollToTop = this.scrollToTop.bind(this);
  }

	  scrollStep = (event) => {
	    if (window.pageYOffset === 0) {
	        clearInterval(this.state.intervalId);
	    }
	    window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
	  }
  
  scrollToTop = (event) => {
    let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
    this.setState({ intervalId: intervalId });
  }
	render() {
		const {got2topShow} = this.props; 
		var clsName = this.props.got2topShow == false ? 'hide' : ' '; 
		return ( 
			<div class={`back-top-Wrap __go2Top  ${clsName}`} onClick={this.scrollToTop}>
				<span></span>
			</div>
		);
	}
}

export default TopArrow;