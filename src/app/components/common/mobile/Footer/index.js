import React 	         		from 'react';  
import { footerLinks }		from '../../../../config/constants';

import './component.css';

class Footer extends React.Component {
	render() { 
		return (
			<div id="footer">
				<div class="footer-links">
					<a href="/">Home</a>
					<a href="/legal">Legal</a>
					<a href="/do/Company/sitemap">Sitemap</a>
					<a href="javascript:void(0)">Desktop Site</a>
					<a href="/do/Company/feedback">Send Feedback</a>
				</div>
				<div class="footer-groupInfo">A naukri.com group website</div>
				<div class="footer-copyright">Copyright © 2017 Info Edge India Ltd. All rights reserved</div>
			</div> 
		);
	}
}

export default Footer;