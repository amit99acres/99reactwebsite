import React, { Component } from 'react';
import { Link } from 'react-router';

class Hamburger extends Component {

  render() {
  			
    return (  

    	<aside id="ham" className="">
    <div className="head">
        <div data-imgCommon='true' className="userIconBox bgSize24_c icn_userLog"></div>
        <div id="login-regiter" className="__clickLyr">Login/Register</div>
    </div>
    <div className="hamContent">
        <div id="siteLinks" className="siteLinks">
            <ul>
                <li>
                    <i data-imgCommon='true' className="bgSize24_c icn_99"></i>
                    <Link className="ripple colorBlue" href="/home-page">Home</Link>
                </li>
                <li>
                <i data-imgCommon='true' className="bgSize24_c icn_shortlistHm"></i>
                    <a className="ripple colorBlue" href="#">Shortlist</a>
                </li>
            </ul>
            
            <div className="catName">Search</div>
            <ul>
                <li>
                    <Link className="ripple colorBlue" href="/search-page">Search Residential</Link>
                </li>
                <li className="active">
                    <Link className="ripple colorBlue" href="/property-detail-page">Search Commercial/PD Page</Link>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Search New Projects</a>
                </li>
            </ul>
            <div className="catName">Services</div>
            <ul>
                <li>
                    <a className="ripple colorBlue" href="#">Sell/Rent Property</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Buy Our Services</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Ask & Answer</a>
                </li>
            </ul>


            

            <div className="catName">Help</div>
            <ul>
                <li>
                    <a className="ripple colorBlue" href="#">Frequently Asked Questions</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Send Feedback</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Buyer Guide</a>
                </li>
            </ul>
            <div className="catName">Quick Links</div>
            <ul>
                <li>
                    <a className="ripple colorBlue" href="javascript:void(0)" id="HotProjects-Link">Hot Projects</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="javascript:void(0)" id="HotAreas-Link">Hot Areas</a>
                </li>
            </ul>

            <div className="catName">Others</div>
            <ul>
                <li>
                    <a className="ripple colorBlue" href="#">Legal</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Sitemap</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Request Desktop Site</a>
                </li>
            </ul>
        </div>

        <div className="hotProjects" id="hotProjects">
            <div className="quickLinks active">
                <a className="ripple colorBlue" href="#">Hot Projects</a>
                <a className="ripple colorBlue clickToback" href="javascript:void(0)" id="hotProjects-siteLinks">back</a>
            </div>
            <div className="catName">Quick Links</div>
            <ul>
                <li>
                    <a className="ripple colorBlue __showHpCityHandler" href="javascript:void(0)" id="Delhi">Delhi</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue __showHpCityHandler" href="javascript:void(0)" id="Mumbai">Mumbai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Bangalore</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Hydrabad</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Kolkata</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Chennai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Pune</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
        </div>
        <div className="hotProjects hotProjectsInCity" id="hotProjectsInCityDelhi">
            <div className="quickLinks active">
                <a className="ripple colorBlue" href="#">Delhi</a>
                <a className="ripple colorBlue clickToback" href="javascript:void(0)" id="hotProjectsInCityDelhi-hotProjects">back</a>
            </div>
            <div className="catName">Quick Links</div>
            <ul>
                <li>
                    <a className="ripple colorBlue" href="#">Saya Gold Avenue</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Godrej Aria</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Omaxe Height</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Saya Gold Avenue</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Godrej Aria</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
        </div>

        <div className="hotProjects hotProjectsInCity" id="hotProjectsInCityMumbai">
            <div className="quickLinks active">
                <a className="ripple colorBlue" href="#">Mumbai</a>
                <a className="ripple colorBlue clickToback" href="javascript:void(0)" id="hotProjectsInCityMumbai-hotProjects">back</a>
            </div>
            <div className="catName">Quick Links</div>
            <ul>
                <li>
                    <a className="ripple colorBlue" href="#">Mumbai Gold Avenue</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Godrej Aria</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Omaxe Height</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Saya Gold Avenue</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Godrej Aria</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
        </div>


        <div className="hotProjects" id="hotAreas">
            <div className="quickLinks active">
                <a className="ripple colorBlue" href="#">Hot Areas</a>
                <a className="ripple colorBlue clickToback" href="javascript:void(0)" id="hotAreas-siteLinks">back</a>
            </div>
            <div className="quickLinksResidential">
                <select id="selectResidential">
                    <option value="0" defaultValue="0">Residential Buy</option>
                    <option value="1">Residential Rent</option>
                    <option value="2">Commercial Buy</option>
                    <option value="3">Commercial Rent</option>
                </select>
            </div>
            <div className="catName">Quick Links</div>
            <ul className="quikLinkListResidential" id="div0">
                <li>
                    <a className="ripple colorBlue __showbhkHandler" href="javascript:void(0)" id="pune">Pune</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                    <ul id="pune-city">
                        <li><a className="ripple colorBlue" href="#">1 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">2 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">3 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">4 BHK</a></li>
                    </ul>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Delhi</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Bangalore</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Hydrabad</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Kolkata</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Chennai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Mumbai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
            <ul className="quikLinkListResidential" id="div1">
                <li>
                    <a className="ripple colorBlue __showbhkHandler" href="javascript:void(0)" id="mumbai">Mumbai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                    <ul id="mumbai-city">
                        <li><a className="ripple colorBlue" href="#">1 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">2 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">3 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">4 BHK</a></li>
                    </ul>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Delhi</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Bangalore</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Hydrabad</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Kolkata</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Chennai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Pune</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
            <ul className="quikLinkListResidential" id="div2">
                <li>
                    <a className="ripple colorBlue __showbhkHandler" href="javascript:void(0)" id="hydrabad">Hydrabad</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                    <ul id="hydrabad-city">
                        <li><a className="ripple colorBlue" href="#">1 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">2 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">3 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">4 BHK</a></li>
                    </ul>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Mumbai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Bangalore</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Delhi</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Kolkata</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Chennai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Pune</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
            <ul className="quikLinkListResidential" id="div3">
                <li>
                    <a className="ripple colorBlue __showbhkHandler" href="javascript:void(0)" id="chennai">Chennai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                    <ul id="chennai-city">
                        <li><a className="ripple colorBlue" href="#">1 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">2 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">3 BHK</a></li>
                        <li><a className="ripple colorBlue" href="#">4 BHK</a></li>
                    </ul>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Mumbai</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Bangalore</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Hydrabad</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Kolkata</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Delhi</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
                <li>
                    <a className="ripple colorBlue" href="#">Pune</a>
                    <a className="ripple colorBlue" href="#">View All</a>
                </li>
            </ul>
            <ul id="genLyr" className="__opnLyr lognOpt">
                <li><a className="ripple colorBlue" href="#">Profile</a></li>
                <li><a className="ripple colorBlue" href="#">Log Out</a></li>
            </ul>
        </div>


        <div className="footer">
            <div>
                <a href='https://play.google.com/store/apps/details?id=com.nnacres.app&utm_source=mobile_pwa&utm_campaign=pwa_ham&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img width='135' alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
            </div>
        </div>
    </div>
</aside>
    	);
  }
}

export default Hamburger;