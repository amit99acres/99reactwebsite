import React          from 'react'; 

import './header.css';
import './hamburger.css';

class Header extends React.Component {
  render() {
    return ( 
       <header class="mobHead">
        <div class="hamIcon" id="hamburger">
          <i data-imgCommon='true'  class="bgSize24_c icn_ham"></i>
        </div>
        <div class="logo99">
          <img width="80" src="https://www.99acres.com/pwa/public/images/logo-icon.png" />
          
        </div>
       
      
      </header>
    );
  }
}

export default Header;