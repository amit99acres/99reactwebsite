import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

class AboutProperty extends React.Component {
  static propTypes = {
    data: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string.isRequired,
      limit: PropTypes.number
    })
  };
  
	render() {
    const {title='About Property', description, limit} = this.props.data;
    if(description == undefined)
      return null;
          
    return ( 
      <div class="pdBlock">
        <h2>{title}</h2>
        <div class="pdDescription">
          {ReactHtmlParser(description)}
        </div>
      </div>
    );
  }
} 

export default AboutProperty;