import React                    from 'react'; 
import PropTypes                from 'prop-types';

class AdditionalDetails extends React.Component {
  static propTypes = {
    showTitle: PropTypes.bool,
    data: PropTypes.shape({
      title: PropTypes.string,
      list: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string,
          type: PropTypes.string,
          id: PropTypes.string
        })
      ).isRequired
    })
  };

	render() {
    const {showTitle=true} = this.props;
    const {title='Additional Details', list} = this.props.data;
    let heading = (showTitle) ? <h2>{title}</h2> : null;

    if(typeof list == 'undefined' || list.length == 0)
      return null;

    const liNode = list.map((amenity) => {
      const showInfoIcon = ((amenity.id == 21 || amenity.id == 6) && showTitle);
      const showURL = (amenity.id == 2443 && showTitle);

      return (<li key={amenity.id}>
        <i class={amenity.class + ' amnIcons'}></i>
        <span>{amenity.label}</span>: 
        <span> {amenity.type} 
          {showInfoIcon ? <i class="iconPd info"></i> : null}
        </span>
        {showURL ? <div class="propCodeUrl">www.99acres.com/V37379473</div> : null}
      </li>)
    });

		return ( 
      <div id="additionalDetails" class="pdBlock"> 
        {heading}
        <ul class="listNone">
          {liNode}
        </ul>
      </div>
    );
  }
}

export default AdditionalDetails;