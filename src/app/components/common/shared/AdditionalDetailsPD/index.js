import React                    from 'react'; 
import PropTypes                from 'prop-types';
import './component.css';

class AdditionalDetails extends React.Component {
  static propTypes = {
    isDesktop: PropTypes.bool,
    data: PropTypes.shape({
      title: PropTypes.string,
      list: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string,
          type: PropTypes.string,
          id: PropTypes.string
        })
      ).isRequired
    })
  };

	render() {
    const {isDesktop=true} = this.props;
    const {title='Additional Details', list} = this.props.data;

    if(typeof list == 'undefined' || list.length == 0)
      return null;

    const liNode = list.map((amenity) => {
      const showInfoIcon = ((amenity.id == 21 || amenity.id == 6) && isDesktop);
      const showURL = (amenity.id == 2443 && isDesktop);

      return (<li key={amenity.id}>
        {isDesktop && <i class={amenity.class + ' amnIcons'}></i> }
        <span>{amenity.label}</span>{isDesktop && (':') }
        <span> {amenity.type}
          {showInfoIcon && <i class="iconPd info"></i>}
        </span>
        {showURL && <div class="propCodeUrl">www.99acres.com/V37379473</div> }
      </li>)
    });

		return ( 
      <div id="additionalDetails" class="pdBlock"> 
        {!isDesktop && <h2>{title}</h2>}
        <ul class="listNone">
          {liNode}
        </ul>
      </div>
    );
  }
}

export default AdditionalDetails;