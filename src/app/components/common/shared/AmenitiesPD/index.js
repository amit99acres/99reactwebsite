import React                    from 'react'; 
import PropTypes                from 'prop-types';
import './component.css';

class Amenities extends React.Component {
  static propTypes = {
    hideTitle: PropTypes.bool,
    data: PropTypes.shape({
      title: PropTypes.string,
      list: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string,
          class: PropTypes.string,
          id: PropTypes.string
        })
      ).isRequired
    })
  };

	render() { 
    const {hideTitle=false} = this.props;
    const {title='Features', list} = this.props.data;
    let heading = (hideTitle) ? null : <h2>{title}</h2>;

    if(typeof list == 'undefined' || list.length == 0)
      return null;

    const liNode = list.map((amenity) => {
      return (<li key={amenity.id}>
        <i class={amenity.class + ' amnIcons'}></i>
        <div>{amenity.label}</div>
      </li>)
    });

		return ( 
      <div id="features" class="pdBlock">
        {heading}
        <ul class="listNone">
          {liNode}
        </ul>
      </div>  
    );
  }
}

export default Amenities;