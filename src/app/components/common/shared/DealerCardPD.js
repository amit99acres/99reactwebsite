import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

class DealerCard extends React.Component {
  static propTypes = {
    data: PropTypes.shape({
      only_name_label: PropTypes.string,
      company_label: PropTypes.string,
      company_profile: PropTypes.string,
      class_name_label: PropTypes.string,
      Url: PropTypes.string,
      Havelogo: PropTypes.string,
      dealer_localities: PropTypes.string,
      address1_label: PropTypes.string,
      address2_label: PropTypes.string,
      city_label: PropTypes.string
    })
  };

	render() {
    const {
      only_name_label
      , company_label
      , company_profile
      , class_name_label
      , Url
      , Havelogo
      , dealer_localities
      , address1_label
      , address2_label
      , city_label
    } = this.props.data;
    
    const dealerPic = (Havelogo == 'N') ? 'https://static.99acres.com/images/pnava.gif' : 'test.jpg';

    if(this.props.data == undefined)
      return null;

		return ( 
      <div class="pdBlock"> 
        <div class="clrAftr">
          <img class="fL dealerPic" alt={class_name_label + ' Info'} src={dealerPic} />
          <div class="primaryInfo">
            <a href="" target="_blank">{only_name_label}</a>
            <div class="company">{company_label}</div>
            <input type="submit" value="View Phone Number" class="btn99 btnTurqLight" />
          </div> 
        </div>
        <div class="infoSet propInfo">
          <a class="" target="_blank" href="">
            Properties Listed: 
            <span>0000000</span>
          </a>
          <a class="" target="_blank" href="">
            Verified Properties: 
            <span>0000000</span>
          </a>
        </div>
        { dealer_localities ? (
          <div class="infoSet">
            <span class="sBold">Localities : </span> {ReactHtmlParser(dealer_localities)}
          </div>
          ) : null 
        } 
        { company_profile ? (
          <div class="infoSet">
            <span class="sBold">About {company_label} <br /></span> {ReactHtmlParser(company_profile)}
          </div>
          ) : null 
        }  
        <div class="infoSet">
          <span class="sBold">Address : </span> {ReactHtmlParser(address1_label)}, {ReactHtmlParser(address2_label)}, {ReactHtmlParser(city_label)}
        </div>
        { Url ? (
          <div>
            Website : <a target="_blank" href={Url} rel="nofollow" class="bLink"> {Url}</a>
          </div>
          ) : null 
        } 
      </div>
    );
  }
}

export default DealerCard;