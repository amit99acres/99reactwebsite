import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ReactHtmlParser          from 'react-html-parser';

const errorStyle = {
  fontSize: '12px',
  color: 'red',
  margin: '3px 0'
};

class ErrorRow extends React.Component {
  static propTypes = {
    data: PropTypes.string.isRequired
  };
  
	render() {
    const {data} = this.props;
    if(data == undefined || data == '')
      return null;
          
    return ( 
      <div style={errorStyle}>
        {ReactHtmlParser(data)}
      </div>
    );
  }
} 

export default ErrorRow;