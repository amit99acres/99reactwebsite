import React          from 'react'; 
import './component.css';

class FactTable extends React.Component {
	render() {
		return ( 
      <ul id="factTable" class="listNone clrAftr">
        <li>
          <div class="head"><i class="iconPd areaIcon"></i> Area</div>
          <div class="details">Super Built up area 550</div>
        </li>
        <li>
          <div class="head"><i class="iconPd configIcon"></i> Configuration</div>
          <div class="details">1 Bedroom, <span class="secondPart">1 Bathroom, 1 Balcony</span></div>
        </li>
        <li>
          <div class="head"><i class="iconPd priceIcon"></i> Price</div>
          <div class="details">₹ 15.99 Lac <span class="f12">(Negotiable)</span><br /> @ 2,908 per Sq.Ft.</div>
        </li>
        <li>
          <div class="head"><i class="iconPd addressIcon"></i> Address</div>
          <div class="details">Krishna Apartment<br /> Sector-70, Noida</div>
        </li>
        <li>
          <div class="head"><i class="iconPd floorIcon"></i> Floor Number</div>
          <div class="details">1st of 6 Floors</div>
        </li>
        <li>
          <div class="head"><i class="iconPd directionIcon"></i> Facing</div>
          <div class="details">North-East</div>
        </li>
        <li>
          <div class="head"><i class="iconPd overlookingIcon"></i> Overlooking</div>
          <div class="details">Main Road</div>
        </li>
        <li>
          <div class="head"><i class="iconPd possessionIcon"></i> Possession In</div>
          <div class="details">Within 3 months</div>
        </li>
      </ul>
    );
  }
}

export default FactTable;