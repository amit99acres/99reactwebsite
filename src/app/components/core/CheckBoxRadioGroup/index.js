import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ErrorRow                 from '../../common/shared/ErrorRow';
import './component.css';

class CheckBoxRadioGroup extends React.Component {
  static defaultProps = {
    type:"radio"
  }

  static propTypes = {
    constraints: PropTypes.shape({
      required: PropTypes.bool
    }),
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.any, 
        id: PropTypes.string.isRequired
      })
    ).isRequired,
    type: PropTypes.oneOf(['checkbox', 'radio']).isRequired,
    value: PropTypes.string,
    changeFunc: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      error:''
    }
  } 

  handleChange = (e, constraints) => {
    // let validField = !0;
    // if(constraints != undefined) {
    //   const validationResponse = validateField(e.target, constraints);
    //   validField = validationResponse.status;

    //   console.log('validationResponse => ', validationResponse);
    //   const errorMsg = (!validField) ? validationResponse.errorMsg : '';
    //   this.setState({error:errorMsg});
    // }
    console.log('radio change =>', e.target.name, e.target.value, constraints);
    this.props.changeFunc(e);
  } 

  render() {
    const {constraints, label, name, options, type} = this.props;
    
    console.log('radio render =>', this.props, constraints);
    return ( 
      <div class="radio-group">
        <div>
          { (typeof label != 'undefined') && <label class="form-label">{label}</label> }
          { options.map((option, index) => (
            <div key={index}>
              <input
                disabled={option.disabled}
                id={name+option.id}
                name={name}
                value={option.id}  
                type={type}
                onChange= {(event) => { this.handleChange(event, constraints) }} 
                />
              <label for={name+option.id}>
                <span></span>
                {option.label}
              </label>
            </div>
          )) } 
        </div>
        <ErrorRow data={this.state.error} />
      </div>
    );
  }
} 

export default CheckBoxRadioGroup;