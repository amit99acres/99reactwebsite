import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ErrorRow                 from '../../common/shared/ErrorRow';
import {
  validateField
  , isNumber
  }                             from '../../../services/utility';
import './component.css';

class CustomInput extends React.Component {
  static propTypes = {
    constraints: PropTypes.shape({
      required: PropTypes.bool,
      minLength: PropTypes.number,
      maxLength: PropTypes.number
    }),
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    blurFunc: PropTypes.func,
    changeFunc: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      error:''
    }
  } 

  handleBlur = (e, constraints) => {
    let validField = !0;
    if(typeof this.props.blurFunc === 'function')
      this.props.blurFunc(e);
    if(constraints != undefined) {
      const validationResponse = validateField(e.target, constraints);
      validField = validationResponse.status;

      console.log('validationResponse => ', validationResponse);
      const errorMsg = (!validField) ? validationResponse.errorMsg : '';
      this.setState({error:errorMsg});
    }
    console.log('blur =>', constraints, e.target.name, validField); 
  }

  handleChange = (e, constraints) => {
    let validField = !0;
    if(constraints != undefined) {
      const validationResponse = validateField(e.target, constraints);
      validField = validationResponse.status;

      console.log('validationResponse => ', validationResponse);
      const errorMsg = (!validField) ? validationResponse.errorMsg : '';
      this.setState({error:errorMsg});
    }
    console.log('change =>', constraints, e.target.name, validField);
  }

  handleKeyPress = (e, constraints) => {
    console.log('keypress called', e, isNumber(e));
    return isNumber(e);
  }

  render() {
    // console.log('Input props =>', this.props, this.state);
    const {constraints, label, name, placeholder} = this.props;
    const labelStr = (typeof label != 'undefined') ? <label class="form-label">{label}</label> : null;
    
    return ( 
      <div class={'text-group ' + (this.state.error.length ? 'errorField' : '')}>
        <div>
          {labelStr}
          <input
            name={name}
            //value={this.props.value}
            minLength={constraints.minLength}
            maxLength={constraints.maxLength}
            placeholder={placeholder}
            type="text"
            onBlur= {(event) => { this.handleBlur(event, constraints) }}
            onChange= {(event) => { this.handleChange(event, constraints) }}
            onKeyPress= {(event) => { this.handleKeyPress(event) }}
            />
        </div>
        <ErrorRow data={this.state.error} />
      </div>
    );
  }
} 

export default CustomInput;