import React                    from 'react'; 
import PropTypes                from 'prop-types';
import ErrorRow                 from '../../common/shared/ErrorRow';
import { validateField }        from '../../../services/utility';
import './component.css';

class customTextarea extends React.Component {
  static defaultProps = {
    showCharLimit:!0
  }

  static propTypes = {
    constraints: PropTypes.shape({
      required: PropTypes.bool,
      minLength: PropTypes.number,
      maxLength: PropTypes.number
    }),
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    rows: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    value: PropTypes.string,
    blurFunc: PropTypes.func,
    changeFunc: PropTypes.func
  };

  constructor(props) {
    super(props);
    console.log('textarea props =>', props.constraints);
    this.state = {
      error:''
      , charText:props.constraints.maxLength + ' chars'
    }
  } 

  handleBlur = (e, constraints) => {
    console.log('textarea blur =>', constraints, e.target.name, e.target.value);
    this.props.blurFunc(e);
  } 

  handleChange = (e, constraints) => {
    let validField = !0;
    if(constraints != undefined) {
      const validationResponse = validateField(e.target, constraints);
      validField = validationResponse.status;

      console.log('validationResponse => ', validationResponse);
      const errorMsg = (!validField) ? validationResponse.errorMsg : '';
      if(this.props.showCharLimit)
        this.updateCharactersLeft(e.target, constraints.maxLength);
      this.setState({error:errorMsg});
    }
    console.log('change =>', constraints, e.target.name, validField);
  }  

  updateCharactersLeft = (elem, limit) => {
    console.log('updateCharactersLeft called');
    let charLeft = parseInt(limit) - elem.value.length;
    charLeft = (charLeft < 0) ? 0 : charLeft;
    this.setState({
      charText: charLeft + ' chars'
    });
  }

  render() {
    const {constraints, label, name, placeholder, rows=6, showCharLimit } = this.props;
    const labelStr = (typeof label != 'undefined') ? <label class="form-label">{label}</label> : null;
    const charTextStr = (showCharLimit) ? <div class="charLimit">{this.state.charText}</div> : null;
    console.log('textarea =>', this.props, constraints);
    return ( 
      <div class="textarea-group">
        <div>
          { labelStr } 
          <textarea 
            name={name}
            placeholder={placeholder}
            maxLength={constraints.maxLength}
            rows={rows} 
            onChange= {(event) => { this.handleChange(event, constraints) }} 
            />
        </div>
        <ErrorRow data={this.state.error} />
        { charTextStr } 
      </div>
    );
  }
} 

export default customTextarea;