export const footerLinks = {
  "col2": {
    "title": "Links",
    "list": [
      { 
        "FINAL_LABEL": "Mobile Apps",
        "URL": "/mobile-apps"
      },
      { 
        "FINAL_LABEL": "National Home",
        "URL": "/Home-Real-Estate.htm"
      },
      { 
        "FINAL_LABEL": "Buy Our Services",
        "URL": "/do/buyourservices"
      },
      { 
        "FINAL_LABEL": "Residential Property",
        "URL": "/property-real-estate-buy-rent-residential"
      },
      { 
        "FINAL_LABEL": "Commercial Property",
        "URL": "/property-real-estate-buy-rent-commercial"
      },
      { 
        "FINAL_LABEL": "Price Trends",
        "URL": "/real-estate-property-rates-index"
      },
      { 
        "FINAL_LABEL": "NRI Services",
        "URL": "/NRI-Real-Estate.htm"
      },
      { 
        "FINAL_LABEL": "Services In India",
        "URL": "/Home-Real-Estate.htm"
      },
      { 
        "FINAL_LABEL": "New Projects",
        "URL": "/new-projects-in-india"
      },
      { 
        "FINAL_LABEL": "Advertise your property",
        "URL": "/postproperty/"
      },
      { 
        "FINAL_LABEL": "Sitemap",
        "URL": "/do/Company/sitemap"
      },
      { 
        "FINAL_LABEL": "Get Alerts",
        "URL": "/do/buyer/ShowBuyerForms/DisplayForm"
      },
      { 
        "FINAL_LABEL": "Builders in India",
        "URL": "https://builders.99sanity.infoedge.com/"
      },
      { 
        "FINAL_LABEL": "Articles",
        "URL": "/articles"
      }
    ]
  }
  , "col3": {
    "title": "Company",
    "list": [
      { 
        "FINAL_LABEL": "About Us",
        "URL": "http://www.infoedge.in"
      },
      { 
        "FINAL_LABEL": "Contact Us",
        "URL": "/load/Company/contactUs"
      },
      { 
        "FINAL_LABEL": "Careers with Us",
        "URL": "http://careers.99acres.com/"
      },
      { 
        "FINAL_LABEL": "Terms &amp; Conditions",
        "URL": "/load/Company/termsconditions"
      },
      { 
        "FINAL_LABEL": "Request Info",
        "URL": "/load/Company/requestinfo"
      },
      { 
        "FINAL_LABEL": "Feedback",
        "URL": "/load/Company/feedback"
      },
      { 
        "FINAL_LABEL": "Report a problem",
        "URL": "/load/Company/feedback"
      },
      { 
        "FINAL_LABEL": "Testimonials",
        "URL": "/load/Company/testimonial"
      },
      { 
        "FINAL_LABEL": "Privacy Policy",
        "URL": "/load/Company/privacy"
      },
      { 
        "FINAL_LABEL": "Summons/Notices",
        "URL": "/load/Company/summons_notices"
      },
      { 
        "FINAL_LABEL": "Grievances",
        "URL": "/load/Company/grievances"
      },
      { 
        "FINAL_LABEL": "Safety Guide",
        "URL": "/load/Company/safetyguide"
      }
    ]
  }
  , "col4": {
    "title": "Our Partners",
    "list": [
      { 
        "FINAL_LABEL": "Naukri.com - Jobs in India",
        "URL": "https://www.naukri.com"
      },
      { 
        "FINAL_LABEL": "Naukrigulf.com - Jobs in middle east",
        "URL": "https://www.naukrigulf.com"
      },
      { 
        "FINAL_LABEL": "Jeevansathi.com - Matrimonials",
        "URL": "https://www.jeevansathi.com"
      },
      { 
        "FINAL_LABEL": "Brijj.com - Professional Networking",
        "URL": "http://www.brijj.com"
      },
      { 
        "FINAL_LABEL": "Shiksha.com - Education Career Info",
        "URL": "https://www.shiksha.com"
      },
      { 
        "FINAL_LABEL": "Policybazaar.com - Insurance India",
        "URL": "https://www.policybazaar.com"
      },
      { 
        "FINAL_LABEL": "Meritnation.com - Online Educational Assessment",
        "URL": "https://www.meritnation.com"
      },
      { 
        "FINAL_LABEL": "Buy Property in India",
        "URL": "http://www.allcheckdeals.com/"
      },
      { 
        "FINAL_LABEL": "PaisaBazaar.com",
        "URL": "https://www.paisabazaar.com/"
      },
      { 
        "FINAL_LABEL": "AmbitionBox.com",
        "URL": "https://www.ambitionbox.com/"
      }
    ]
  }
}