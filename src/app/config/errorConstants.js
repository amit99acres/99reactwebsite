const errorConstants = {
  EMAIL_REQUIRED: 'Valid Email ID is required eg: rkumar@gmail.com.'
  , EMAIL_MINLENGTH: 'Email should be minimum 8 characters eg: rkumar@gmail.com.'
  , EMAIL_MAXLENGTH: 'Email should be 50 characters or less.'
  , EMAIL_ISVALIDEMAIL: 'Please specify a valid Email ID eg: rkumar@gmail.com.' 
  , IDENTITY_REQUIRED: 'Please select are you an Individual or Dealer.'
  , MOBILE_REQUIRED: 'Mobile no. is required.'
  , MOBILE_MINLENGTH: 'Please specify a valid mobile no.'
  , NAME_REQUIRED: 'Please enter your name.'
  , NAME_MINLENGTH: 'Name must be atleast 3 characters long.'
  , NAME_MAXLENGTH: 'Name should be 25 characters or less.'
  , PHONE_REQUIRED: 'Please enter your contact number.'
  , PHONE_MINLENGTH: 'Mobile entered is invalid.'
}

export default errorConstants;