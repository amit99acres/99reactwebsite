import fetch      from 'isomorphic-fetch';

const requestPage = () => ({ type: 'FETCH_PAGE_REQUEST' });
const receivedPageData = data => ({ type: 'FETCH_PAGE_SUCCESS', data: data });
const fetchError = (err) => ({ type: 'FETCH_PAGE_FAILURE' });

export const fetchPageData = () => (dispatch, getState) => {
  dispatch(requestPage());

  return fetch("http://localhost:8888/data/pd.json")
    .then(response => response.json())
    .then(data => dispatch(receivedPageData(data)))
    .catch(err => dispatch(fetchError(err)));
    //.catch(err => {console.log('err =>', err)});
};