import React, { Component } 		from 'react'; 
import { Link }                 from "react-router-dom";

import ErrorBoundary						from '../../components/common/ErrorBoundary';
import Header 									from '../../components/common/desktop/Header';

class Home extends Component {  
	render() {
		return (
			<div>
				<ErrorBoundary>	
					<Header /> 
				</ErrorBoundary>
				<div class="contentWrap"> 
          <p>
            <Link to="/pd/22">PD Page</Link>
          </p>
           <p>
            <Link to="/search/222">Search Page</Link>
          </p>
				</div>  
			</div>
		)
	}
}

export default Home;