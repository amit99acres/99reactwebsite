import React, { Component } 		from 'react'; 

import ErrorBoundary						from '../../components/common/ErrorBoundary';
import Header 									from '../../components/common/desktop/Header';
import FactTable								from '../../components/common/shared/FactTablePD';
import AdditionalDetails				from '../../components/common/shared/AdditionalDetailsPD';
import AboutProperty						from '../../components/common/shared/AboutPropertyPD';
import Amenities								from '../../components/common/shared/AmenitiesPD';
import DealerCard								from '../../components/common/shared/DealerCardPD';
import OtherLinks								from '../../components/PD/desktop/OtherLinks';
import AskAnswer								from '../../components/common/desktop/AskAnswer';
import Footer 									from '../../components/common/desktop/Footer';
import DealerForm 							from '../../components/EOI/DealerForm/';

class Layout extends React.Component {
	render() {
		console.log('Desktop props = ', this.props, this.state);
		const { pageData } = this.props;
		return ( 
      <div>
				<ErrorBoundary>	
					<Header /> 
				</ErrorBoundary>
				<div class="contentWrap">
					<div>
						<div class="clrAftr contentInnerWrap navWrapper">
							<div class="breadCrumb fL">
								<a href="" title="Home">Home </a> <span class="rPin">›</span><a href="" title="Property in Noida">Property in Noida </a> <span class="rPin">›</span> <a href="" title="Sector-121 Noida">Sector-121 Noida </a>
							</div>
							<div class="fR info">
								<span class="pdPropDate">Posted on Jul 02, 2018</span>
								<span>Under Construction</span>
							</div>
						</div>
						<ul id="tabs" class="listNone contentInnerWrap">
							<li class="sel">Overview</li>
							<li>Dealer Details</li>
							<li>Recommendations</li>
						</ul>
					</div>
					<div>
						<div class="contentInnerWrap">
							<div id="galleryFact">
								<ErrorBoundary>
									<FactTable />
								</ErrorBoundary>
							</div>
							<ErrorBoundary>
								<AdditionalDetails data={pageData.AdditionalData} />
							</ErrorBoundary>
							<ErrorBoundary>
								<AboutProperty data={pageData.AboutData} />
							</ErrorBoundary>
							<ErrorBoundary>
								<Amenities data={pageData.Features} />
							</ErrorBoundary>
						</div>
					</div>
					<div>
						<div class="contentInnerWrap clrAftr" id="dealerInfo">
							<ErrorBoundary>
								<DealerCard data={pageData.ContactDetails} />
							</ErrorBoundary>
							<div id="enquiryForm" class="fL"> 
								<div class="heading lightGrey">Send enquiry to Dealer</div>
								<ErrorBoundary>
									<DealerForm />
								</ErrorBoundary>
							</div>
						</div>
					</div>
          <ErrorBoundary>	
            <AskAnswer data={pageData.FooterData} /> 
          </ErrorBoundary>	
          <ErrorBoundary>	
            <OtherLinks data={pageData.FooterData} /> 
          </ErrorBoundary>	
				</div>
				<ErrorBoundary>	
					<Footer data={pageData.FooterData} /> 
				</ErrorBoundary>	 
			</div>
		)
	}
}

export default Layout;