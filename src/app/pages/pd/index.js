import React, { Component } 		from 'react'; 
import { connect } 							from 'react-redux'; 
import { fetchPageData }				from './actions'; 

// import Sidepane						from './../../components/common/desktop/Sidepane/';
import DesktopView 						from './desktop';
import MobileView 						from './mobile';

class Layout extends React.Component {	
	static initialAction() {
		console.log('initialAction =>', this.props)
		const parmamsSet = (this.props && this.props.viewType == 'mobile') ? '' : 'all';
		return fetchPageData(parmamsSet);
	} 
	
	componentDidMount() {
		if(!this.props.pageData) 
			this.props.dispatch(Layout.initialAction());
	}

	render() {
		console.log('PD render = ', this.props, this.state);
		const { pageData, viewType } = this.props;
		if(this.props.pageData == undefined)
      return null; 
		return (
			viewType === 'desktop' ?
			<DesktopView pageData={pageData} /> :
			<MobileView pageData={pageData} />
		); 
	}
}

const mapStateToProps = state => ({
	pageData: state.pageData
});

export default connect(mapStateToProps)(Layout); 