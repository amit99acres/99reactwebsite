import React, { Component } 		from 'react'; 

import ErrorBoundary						from '../../components/common/ErrorBoundary';
import Header 									from '../../components/common/desktop/Header';
import FactTable								from '../../components/common/shared/FactTablePD';
import AdditionalDetails				from '../../components/common/shared/AdditionalDetailsPD';
import AboutProperty						from '../../components/common/shared/AboutPropertyPD';
import Amenities								from '../../components/common/shared/AmenitiesPD';
import DealerCard								from '../../components/common/shared/DealerCardPD';
import Footer 									from '../../components/common/mobile/Footer';
import DealerForm 							from '../../components/EOI/DealerForm/';

class Layout extends React.Component {
	render() {
		console.log('Mobile Layout props = ', this.props, this.state);
		const { pageData } = this.props;
		return (
			<div> 
				<div class="contentInnerWrap">
					<div class="pdBlock">
						<ErrorBoundary>
							<FactTable />
						</ErrorBoundary>
					</div>
          <ErrorBoundary>
            <AdditionalDetails data={pageData.AdditionalData} isDesktop={false} />
          </ErrorBoundary>
          <ErrorBoundary>
            <Amenities data={pageData.Features} />
          </ErrorBoundary>
          <ErrorBoundary>
            <AboutProperty data={pageData.AboutData} />
          </ErrorBoundary>
          <ErrorBoundary>
            <DealerCard data={pageData.ContactDetails} isDesktop={false} />
          </ErrorBoundary>
        </div>  
				<ErrorBoundary>
					<Footer />
				</ErrorBoundary>
			</div>
		)
	}
}

export default Layout;