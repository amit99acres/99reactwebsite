import { 
  applyMiddleware, 
  createStore 
  }                         from 'redux';
import { createLogger }		  from 'redux-logger';
import thunk 								from 'redux-thunk';
import promise 							from 'redux-promise-middleware';
import Reducer				      from './Reducer';

const middleware = applyMiddleware(promise(), thunk, createLogger());
const configureStore = preloadedState =>
  createStore(Reducer, preloadedState, middleware);

export default configureStore; 