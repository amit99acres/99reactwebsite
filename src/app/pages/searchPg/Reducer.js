// const initialState = {
// 	viewType: "desktop"
// 	, pageData:{
// 		AdditionalData:{},
// 		AboutData:{},
// 		Features:{},
// 		ContactDetails:{}, 
// 		FooterData:{}
// 	}
// };

const initialState = {};
const Reducer = (state = {}, action) => {
	console.log('PD Reducer called => ', state);
	switch(action.type){
		case 'FETCH_PAGE_REQUEST': 
			return { ...state, fetching:!0 };
		case 'FETCH_PAGE_SUCCESS':
			return { ...state, pageData:action.data, fetching:!1, fetched:!0 };
		case 'FETCH_PAGE_FAILURE': 
			return { ...state, pageData: null, fetching:!1, error:action.data };
	}
	return state;
}; 

export default Reducer; 

// @connect((store) => {
//   return {
//     user: store.user.user
//   };
// }) 

// const initialState = {
// 	fetching: !1
// 	, fetched: !1
// 	, user: {
// 	    id: null
// 	    , name: null
// 	    , age: null
// 	}
// 	, error: null
// };

// return { ...state, news: action.payload };