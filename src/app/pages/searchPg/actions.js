import fetch      from 'isomorphic-fetch';

const requestPage = () => ({ type: 'FETCH_PAGE_REQUEST' });
const receivedPageData = data => ({ type: 'FETCH_PAGE_SUCCESS', data: data });
const fetchError = (err) => ({ type: 'FETCH_PAGE_FAILURE', data: err });

export const fetchPageData = (parmamsSet) => (dispatch, getState) => {
  dispatch(requestPage());

  let params = '?module=a,b,c,d,e,f';
  if(parmamsSet == 'all')
    params += ',g,h,i,j,k';
  let requestURL = "http://localhost:8888/data/srp.json" + params;
  return fetch(requestURL)
    .then(response => response.json())
    .then(data => dispatch(receivedPageData(data)))
    .catch(err => dispatch(fetchError(err)));
    //.catch(err => {console.log('err =>', err)});
};