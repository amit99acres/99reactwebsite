import React, { Component } 		from 'react'; 

import ErrorBoundary						from '../../components/common/ErrorBoundary';
import Header 									from '../../components/common/desktop/Header';
import SrpTuple								from '../../components/Search/desktop/srpTuple';
import LeftFilters   					from '../../components/common/desktop/LeftFilters';
import CommnEllipsis   					from '../../components/common/desktop/Ellipsis';
import SwitchButton							from '../../components/Search/desktop/commonSearch/switchButton/switchButton';
import Ysf							from '../../components/Search/desktop/commonSearch/ysf';
import ListMapView							from '../../components/Search/desktop/commonSearch/listMapView/listMapView';
import SrpBrdCrm 							from '../../components/Search/desktop/commonSearch/srpBrdCrm'
import SrpNudges 							from '../../components/Search/desktop/commonSearch/srpNudges/srpNudges';
import Pagination							from '../../components/Search/desktop/commonSearch/pagination/pagination'
import SendFeedback 									from '../../components/common/desktop/SendFeedback';
import TopArrow 									from '../../components/common/desktop/TopArrow';
import Footer 									from '../../components/common/desktop/Footer';


import './component.css';

class Layout extends React.Component {
	constructor(props) {
	  super(props);
	   this.state={	   
	   	nudgeFix: false,
	   	leftFilter: 'False',
	   	fixcompHg: '0',
	   	filtrStkHg: 'auto',
	   	topShift: 'auto',
	   	go2top: false,
	   	bgLyr: ''
	   }
	   
	 this.onBgOn = this.onBgOn.bind(this);
	 this.onScrollSrp = this.onScrollSrp.bind(this);
	 this.totalFxd = 0;
	 this.topShift;
	 this.temp = false;
	 this.flag = false;
	 
}

componentDidMount(){
		
		let arrfixdComp = [document.querySelector('#LF_TABS').clientHeight];
		for(var k = 0, length3 = arrfixdComp.length; k < length3; k++){
			this.totalFxd  += arrfixdComp[k];
		}
		window.addEventListener('scroll', this.onScrollSrp); 
	}

	onBgOn (status){
        this.setState({bgLyr: status});
        // console.log(status);

  }

onScrollSrp = (event) => {
		 let sticky = window.scrollY;
		// nudge scroll fixed
		 let nudgeTp = document.querySelector('.__srpnudg').offsetTop - document.querySelector('.__srpnudg').clientHeight + 6;
         let nudgeFixValue = sticky >= nudgeTp? true : false;
         if(this.temp != nudgeFixValue){
         	this.temp = nudgeFixValue;
         	this.setState({nudgeFix: nudgeFixValue});          	
         }   
         
         // End -- nudge scroll fixed 

         // left filters scroll fixed          
         	let switchBtnHg = 60, 
         	footrHg = document.querySelector('#footer').clientHeight, 
         	srpTupWrpHg =  document.querySelector('._srpTpl').clientHeight,
         	docHg = document.documentElement.scrollHeight,
         	leftFilterTop = document.querySelector('.lSide-srp-filters').offsetTop - (this.totalFxd),
         	leftFilterFix = sticky > leftFilterTop ? 'True' : 'False',
         	filtrStckHg = window.innerHeight - this.totalFxd,
         	varble = 50;
        	// top fixed
        	let topFxd = sticky > switchBtnHg? true : false;
        	// if(sticky > switchBtnHg){
        		 if(this.flag != topFxd){
         			this.flag = topFxd;
	        		this.setState({go2top: topFxd}); 
	        		this.setState({topShift: '0'}); 
	        		this.setState({fixcompHg: this.totalFxd, leftFilter: leftFilterFix, filtrStkHg: filtrStckHg});
        	}
        	// else if(sticky <= 100){
        	// 	this.setState({ leftFilter: leftFilterFix});
        	// }
        	this.setState({fixcompHg: this.totalFxd, leftFilter: leftFilterFix, filtrStkHg: filtrStckHg});
        	this.topShift = (srpTupWrpHg - footrHg)- this.totalFxd - switchBtnHg - varble;
        	// Bottom fixed
        	if(sticky + footrHg + (docHg - srpTupWrpHg) >= docHg){
        		this.setState({topShift: this.topShift, leftFilter: 'false'}); 
	        } 

         // End -- left filters scroll fixed
	}

	render() {
		let clsName = this.state.bgLyr;
		const { pageData } = this.props;
		return ( 
      		<div class="searchPage" onScroll={this.onScrollSrp}>
				<ErrorBoundary>	
					<Header /> 
				</ErrorBoundary>
				<SrpNudges stickyCondtn={this.state.nudgeFix}  data={pageData.srpNudge}/>
				<CommnEllipsis  onBgOn={this.onBgOn} />
				<div class="contentWrap">
					<div class="spacer10"></div>					
					<div class="srpLeftFilters">
						<SwitchButton />
						<LeftFilters stickyHg={this.state.filtrStkHg} btmTopShift={this.state.topShift} leftFiltrsticky={this.state.leftFilter} fixcomp={this.state.fixcompHg} />
					</div>
					<div class="srpTupleCont">
						<div>
							<SrpBrdCrm />							
							<ListMapView />
							<Ysf />
						</div>
						<div class="_srpTpl">
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						 <SrpTuple data={pageData.srpTuple}/>
						</div>
						<Pagination />
						<div class="spacer10"></div>
					</div>
				</div>
				<SendFeedback onBgOn={this.onBgOn}/>
				<TopArrow got2topShow ={this.state.go2top}/>
				<Footer />
				<div className={`coverBg ${clsName}`}></div>
			</div>
		)
	}
}

export default Layout;