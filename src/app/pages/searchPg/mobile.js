import React, { Component } 		from 'react'; 

import ErrorBoundary						from '../../components/common/ErrorBoundary';
import Mob_Header 									from '../../components/common/mobile/Header';
import Mob_srpNudges								from '../../components/Search/mobile/srpNudges';
import Mob_srpTuple								from '../../components/Search/mobile/srpTuple';
import YsfWrap								from '../../components/Search/mobile/ysf';
// import AboutProperty						from '../../components/common/shared/AboutPropertyPD';
// import Amenities								from '../../components/common/shared/AmenitiesPD';
// import DealerCard								from '../../components/common/shared/DealerCardPD';
import Footer 									from '../../components/common/mobile/Footer';


class Layout extends React.Component {
	render() {
		console.log('Mobile Layout props = ', this.props, this.state);
		const { pageData } = this.props;
		return (
			<div> 
				<ErrorBoundary>
					<Mob_Header />
				</ErrorBoundary>
				<div class="contentInnerWrap">
					<YsfWrap />
					<Mob_srpNudges />
					<Mob_srpTuple />
					<Mob_srpTuple />
					<Mob_srpTuple />
					<Mob_srpTuple />
					<Mob_srpTuple />
          
        		</div>  
				<ErrorBoundary>
					<Footer />
				</ErrorBoundary>
			</div>
		)
	}
}

export default Layout;