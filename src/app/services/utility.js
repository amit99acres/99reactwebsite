import errorConstants           from '../config/errorConstants';

export function validateEmail(email) {
  let regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

export function isNumber(evt) {
  let charCode = evt.which ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) 
    return !1;
  return !0;
}

export function validateMobileNumber(number) {
  return regex.test(number);
}

export function validateField(elem, constraints) {
  let response = {};
  let validField = !0;
  let errorMsg = 'Please enter valid value.';

  const fieldName = elem.name;
  const fieldValue = elem.value;
  const fieldValueLength = fieldValue.length;
  const {minLength, maxLength} = constraints;

  for(var key in constraints) {
    let errorKey;
    const getValidationStatus = {
      'required': function () {
        errorMsg = 'Required';
        return (fieldValueLength > 0) ? !0 : !1;
      },
      'minLength': function () {
        errorMsg = 'Entered value should be ' + minLength + ' characters or more.';
        return (fieldValueLength == 0) ? !0 : (fieldValueLength >= minLength);
      },
      'maxLength': function () {
        errorMsg = 'Entered value should be ' + maxLength + ' characters or less.';
        return (fieldValueLength <= maxLength);
      },
      'isValidEmail': function () {
        errorMsg = 'Please specify a valid Email ID eg: rkumar@gmail.com.';
        return validateEmail(fieldValue);
      },
      'isValidNumber': function () {
        errorMsg = 'Please enter a valid number';
        return validateMobileNumber(fieldValue);
      }
    } 

    validField = getValidationStatus[key]();
    //console.log('validateField =>', key, fieldValue, validField);

    if(!validField) {
      errorKey = fieldName + '_' + key;
      errorKey = errorKey.toUpperCase();
      errorMsg = (errorConstants[errorKey]) ? errorConstants[errorKey] : errorMsg;

      response['errorKey'] = errorKey;
      console.log('Utility errorKey =>', errorKey, errorMsg);
      break;
    }
  }
  
  response['status'] = validField;
  response['errorMsg'] = errorMsg;
  return response;
}