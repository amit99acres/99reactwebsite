import React 							from 'react';
import { 
	render,
	hydrate 
	} 											from 'react-dom';
import { BrowserRouter } 	from 'react-router-dom';
import { Provider } 			from 'react-redux';
import configureStore			from './app/pages/pd/store';
import App 								from './app/App';

import './app/assets/css/reset.css'; 
import './app/assets/css/generic.css'; 
import './app/assets/css/common.css'; 
import './app/assets/css/pd.css'; 

const store = configureStore(window.__initialData__);

const { viewType } = window.__initialData__;
const app = <Provider store={store}>
	<BrowserRouter>
		<App viewType={viewType} />
	</BrowserRouter>
</Provider>

const appNode = document.getElementById('app');
const isAppRendered = appNode.hasChildNodes();
const renderMethod = (isAppRendered) ? hydrate : render;
renderMethod(
	app
	, appNode
);