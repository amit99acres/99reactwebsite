import cors                   from 'cors';
import express                from 'express';
import path                   from 'path';
import React                  from 'react';
import { 
  renderToNodeStream
  , renderToString }          from 'react-dom/server';
import { 
  StaticRouter
  , matchPath }               from 'react-router-dom';
import { Provider } 			    from 'react-redux'; 
import useragent              from 'express-useragent';

import routes                 from './app/Routes';
import App                    from './app/App';
import configureStore			    from './app/pages/pd/store';

const app = new express();
const port = process.env.PORT || 8888;
const env = process.env.NODE_ENV || 'production';

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'tpls'));
app.use(cors());
app.use(useragent.express());
app.use(express.static("public"));

app.use(function(req, res, next) {
  res.locals.ua = req.get('User-Agent'); 
  res.locals.assetsPath = ('production' === app.get('env')) ? 'http://static.99acres.com/js/' : '/';
  next();
});

app.get('*.js', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  next();
});

app.get('*', (req, res, next) => {
  if(req.url == '/favicon.ico')
    return;

  const store = configureStore();
  const promises = routes.reduce((acc, route) => {
    if (matchPath(req.url, route) && route.component && route.component.initialAction) {
      acc.push(Promise.resolve(store.dispatch(route.component.initialAction())));
    }
    return acc;
  }, []);

  Promise.all(promises)
    .then(() => {
      let markup = '';
      let initialState = store.getState();
      let status = 200; 
      const viewType = (req.useragent.isDesktop) ? 'desktop' : 'mobile';
      const context = {};

      if (process.env.UNIVERSAL) {
        markup = renderToString(
          <Provider store={store}>
            <StaticRouter location={req.url} context={context}>
              <App viewType={viewType} />
            </StaticRouter>
          </Provider>
        );

        if(context.url) 
          return res.redirect(302, context.url);
    
        if(context.is404) 
          status = 404;
      }
      
      // console.log('server viewType =>', viewType);
      initialState = Object.assign(initialState, {'viewType':viewType});
      initialState = JSON.stringify(initialState);
      // console.log('initialState =>', initialState);
      let pageData = {markup, initialState};
      return res.status(status).render('index', { pageData });
  }).catch(next);
});

app.listen(port, (err) => {
  if (err) 
    return console.error(err);
  return console.info(
    `😎 Server running on http://localhost:${port} [${env}]
      Universal rendering: [${process.env.UNIVERSAL ? 'enabled' : 'disabled'}]`);  
});