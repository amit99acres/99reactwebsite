import path                     from 'path';
import nodeExternals            from 'webpack-node-externals';
import ExtractText              from 'extract-text-webpack-plugin';
import UglifyJs                 from 'uglifyjs-webpack-plugin';
import MiniCssExtract           from 'mini-css-extract-plugin';
// import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

const devMode = process.env.NODE_ENV !== 'production';
console.log('devMode =>', devMode);

const client = {
  entry: {
    pd: './src/client.js'
    , vendor: ['react', 'react-dom', 'redux', 'react-redux', 'react-html-parser', 'prop-types']
  }
  , output: {
    path: path.join(__dirname, 'public')
		, filename: 'js/[name]Bundle.js'
    , publicPath: path.join('/')
  }
  , optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					chunks: 'initial',
					name: 'vendor',
					test: 'vendor',
					enforce: !0
				},
			}
		}
		, minimizer: [ 
      new UglifyJs({
        cache: !0,
				parallel: !0, 
				extractComments: !0,
        uglifyOptions: {
          ecma: 6,
          mangle: !0
        },
        sourceMap: !0
      })
    ]
	}
  , module: {
    rules: [ 
      {
        test: /\.css$/,
        use: [MiniCssExtract.loader, "css-loader"]
      }
      , {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: "babel-loader"
      }
      , {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      }
    ]
  }
  , plugins: [ 
    new MiniCssExtract({
      filename: "css/style.css",
      chunkFilename: "[id].css"
    })
  ]
};

const server = {
  target: 'node',
  node: {
    __dirname: !1,
  },
  externals: [nodeExternals({
    modulesFromFile: !0,
  })],
  entry: {
    js: './src/server.js',
  },
  output: {
    path: path.join(__dirname, 'src'),
    filename: 'server-es5.js',
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: "css-loader/locals"
          }
        ]
      } 
      , {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
      } 
    ]
  } 
  , plugins: [
    new ExtractText(
      {
      	filename: 'css/style.css'
	      , allChunks: !0
      }
    ) 
    // , new BundleAnalyzerPlugin()
  ]
};

export default [client, server]; 